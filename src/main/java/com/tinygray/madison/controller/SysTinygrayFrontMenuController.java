package com.tinygray.madison.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.tinygray.madison.enmu.Constants;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.service.SysTinygrayFrontMenuService;
import com.tinygray.madison.util.MenuTreeUtil;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 前端菜单表 前端控制器
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
@Api(value = "菜单控制器")
@RestController
@RequestMapping("/tinygray-front-menu")
public class SysTinygrayFrontMenuController extends ApiController {
    @Autowired
    SysTinygrayFrontMenuService sysTinygrayFrontMenuService;
    @Autowired
    MenuTreeUtil menuTreeUtil;
    @ApiOperation("所有菜单列表")
    @GetMapping("selectMenus")
    public R<List<TinygrayFrontMenuVo>> selectMenus() {
        List<TinygrayFrontMenu> tinygrayFrontMenus = sysTinygrayFrontMenuService.selectMenus();
        List<TinygrayFrontMenuVo> tinygrayFrontMenuVoList = menuTreeUtil.selectMenusListForTree(tinygrayFrontMenus);
        return success(tinygrayFrontMenuVoList);
    }
    /**
     * 获取单个菜单信息
     * @param menuId
     * @return
     */
    @GetMapping("/info/{menuId}")
    public R getMenuById(@PathVariable Integer menuId){
        TinygrayFrontMenu tinygrayFrontMenu = sysTinygrayFrontMenuService.getMenuById(menuId);
        return success(tinygrayFrontMenu);
    }

    @PostMapping("/info/saveOrUpdate")
    public R saveOrUpdate(@RequestBody TinygrayFrontMenu tinygrayFrontMenu) {
        tinygrayFrontMenu.setIcon(tinygrayFrontMenu.getIcon().substring(8));
        if (tinygrayFrontMenu.getId() != 0) {
            return success(sysTinygrayFrontMenuService.saveOrUpdate(tinygrayFrontMenu));
        }else {
            return success(sysTinygrayFrontMenuService.save(tinygrayFrontMenu));
        }
    }

    @PostMapping("info/delete")
    public R deleteMenuById(@RequestBody TinygrayFrontMenuVo tinygrayFrontMenuVo) {
        boolean flag = sysTinygrayFrontMenuService.deleteMenuById(tinygrayFrontMenuVo);
        return success(flag);
    }

}
