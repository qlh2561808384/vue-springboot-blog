package com.tinygray.madison.controller.aliPay.util;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.tinygray.madison.controller.aliPay.config.AliPayProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.Map;

/**
 * @Author: qlh
 * @Desctiption: 支付宝签名工具类
 * @Date: 2020/7/25 16:47
 */

/**
 * 为什么加上@Component 而不用@Bean
 *  @Bean
 * 用于方法上，该方法返回一个实例对象告诉Spring，然后在Spring容器中注册成一个bean，通常方法体中包含了最终产生bean实例的逻辑。
 * 主要用于第三方库中的类需要装配到Spring容器，因为无法在第三方库中加@Component注解，只能通过@Bean来实现。
 *
 * @Component
 * 用于类上，将该类注册成一个组件类（bean），并告知Spring要为这个类创建bean。与@Controller、@Service、@Repository的意思一样。
 * 需要通过类路径扫描来自动装配到Spring容器中。@Bean不需要通过类路径扫描
 *
 * 相同
 * 目的一样，都是想当与在xml配置了一个<bean></bean>。
 */
@Slf4j
@Component
public class AliPaySignUtil {

//    @Setter
    @Autowired
    private AliPayProperties payProperties;

    public boolean signVerified(Map<String, String> paramsMap){
        boolean signVerified = false;//调用SDK验证签名
        try {
//            signVerified = AlipaySignature.rsaCheckV1(paramsMap, payProperties.getAlipayPublicKey(), payProperties.getCharset());
            signVerified = AlipaySignature.rsaCheckV1(paramsMap, payProperties.getAlipayPublicKey(), payProperties.getCharset(), payProperties.getSignType());
        } catch (AlipayApiException e) {
            e.printStackTrace();
            log.error("安全签名验证失败："+ JSON.toJSONString(paramsMap),e);
        }
        return signVerified;
    }

}
