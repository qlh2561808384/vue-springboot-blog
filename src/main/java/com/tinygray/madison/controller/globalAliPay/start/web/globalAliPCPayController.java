package com.tinygray.madison.controller.globalAliPay.start.web;

import com.tinygray.madison.controller.globalAliPay.api.GlobalPCPay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Class:
 * @Description: globalAliPCPayController$
 * @title: globalAliPCPayController
 * @Author qlh
 * @Date: 2020/7/24 11:18
 * @Version 1.0
 */
@RestController
@RequestMapping(value = "/globalAliPay/pc/page")
public class globalAliPCPayController {
    @Autowired
    GlobalPCPay globalPCPay;

    @GetMapping("/pay")
    public String globalPCPay(HttpServletResponse response) {
        return globalPCPay.globalPCPay();
    }
}
