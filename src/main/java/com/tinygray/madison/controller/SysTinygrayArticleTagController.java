package com.tinygray.madison.controller;


import com.baomidou.mybatisplus.extension.api.ApiController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 文章标签关系表 前端控制器
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
@RestController
@RequestMapping("/tinygray-article-tag")
public class SysTinygrayArticleTagController extends ApiController {

}
