package com.tinygray.madison.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

@ApiModel(description = "生成随机值的工具类")
public class RandomUtils {
    /**
     * 获取m~n范围内的整数
     *
     * @param m
     * @param n
     * @return
     */
    @ApiOperation(value = "获取m~n范围内的整数")
    public static int getRandom(int m, int n) {
        int random = (int) (Math.random() * (n - m)) + m;
        return random;
    }

    /**
     * 获取位数为n的随机数
     * @param length
     * @return
     */
    @ApiOperation(value = "获取位数为n的随机数")
    public static int getRandom(int length){
        int m=getNumber(length);
        int n=m*10-1;
        int random=(int)(Math.random()*(n-m))+m;
        return random;
    }

    /**
     *
     * @param n
     * @return
     */
    @ApiOperation(value = "处理数字")
    public static int getNumber(int n){
        if(n<1){
            n=1;
        }
        if(n==1){
            return 1;
        }else{
            n=n-1;
            return 10*getNumber(n);
        }
    }
}
