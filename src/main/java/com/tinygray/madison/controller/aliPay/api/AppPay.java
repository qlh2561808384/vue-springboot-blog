package com.tinygray.madison.controller.aliPay.api;

import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.tinygray.madison.controller.aliPay.domain.AppPay.AppPayDTO;

public interface AppPay {
    /**
     *
     * @param appPayDTO
     * @return AlipayTradeAppPayResponse
     * @Desctiption 统一收单下单并支付app接口
     */
    AlipayTradeAppPayResponse pay(AppPayDTO appPayDTO);
}
