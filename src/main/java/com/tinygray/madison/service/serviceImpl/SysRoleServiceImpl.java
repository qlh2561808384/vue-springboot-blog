package com.tinygray.madison.service.serviceImpl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tinygray.madison.enmu.Constants;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.entity.TinygrayRole;
import com.tinygray.madison.mapper.SysRoleMapper;
import com.tinygray.madison.service.SysRoleService;
import com.tinygray.madison.vo.SysRoleAndPermissionVo;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, TinygrayRole> implements SysRoleService {
    private static final String Prefix = "ROLE_";
    @Override
    public List<String> getRolesByUserName(String username) {
        return this.baseMapper.getRolesByUserName(username);
    }

    @Override
    public List<TinygrayRole> getAllRole(String keyword) {
        if (StringUtils.isNotBlank(keyword)) {
            return this.baseMapper.selectList(new QueryWrapper<TinygrayRole>().lambda().like(TinygrayRole::getRolename, keyword));
        }else {
            return this.baseMapper.selectList(new QueryWrapper<TinygrayRole>().lambda());
        }
    }

    @Override
    public boolean addRole(TinygrayRole tinygrayRole) {
        if (StrUtil.hasEmpty(tinygrayRole.getRolename()) || StrUtil.hasEmpty(tinygrayRole.getDescription())) {
            return false;
        } else {
            tinygrayRole.setRolename(Prefix + tinygrayRole.getRolename());
            if (tinygrayRole.getId() == 0) {
                return this.save(tinygrayRole);
            } else {
                return this.saveOrUpdate(tinygrayRole);
            }
        }
    }

    @Override
    public boolean delRole(long id) {
        return this.baseMapper.delRole(id);
    }

    @Override
    public List<TinygrayFrontMenu> getRoleToMenuList(Integer roleId) {
        return this.baseMapper.getRoleToMenuList(roleId);
    }
}
