package com.tinygray.madison.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TinygrayUser implements Serializable {

  private long id;
  private String nickname;
  private String password;
  private String email;
  private Integer status;
  private String createTime;
  private String avatar;
  private String birthday;
  private Integer age;
  private Integer phone;
}
