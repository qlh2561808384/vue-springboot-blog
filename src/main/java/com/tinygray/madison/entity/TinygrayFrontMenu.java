package com.tinygray.madison.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 前端菜单表
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TinygrayFrontMenu对象", description="前端菜单表")
public class TinygrayFrontMenu extends Model<TinygrayFrontMenu> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "前端菜单表id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "前端菜单名称name")
    private String frontMenuName;

    @ApiModelProperty(value = "前端菜单路径url")
    private String frontMenuUrl;

    @ApiModelProperty(value = "树形结构pid => 一级菜单、二级菜单、三级菜单")
    private Integer pid;

    @ApiModelProperty(value = "菜单描述")
    private String description;

    @ApiModelProperty(value = "菜单图标icon")
    private String icon;

    private Integer frontMenuSort;

    @ApiModelProperty(value = "对应前端路由path")
    private String path;

    @ApiModelProperty(value = "类型：0表示目录1表示菜单")
    private Integer type;

    @TableField(exist=false)
    private Integer rodeId;
    /**
     * 父菜单名称
     */
    @TableField(exist=false)
    private String parentName;
    /**
     * z-tree属性
     */
    @TableField(exist=false)
    private Boolean open;

    @TableField(exist=false)
    private List<?> list;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
