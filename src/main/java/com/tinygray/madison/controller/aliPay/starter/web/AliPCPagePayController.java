package com.tinygray.madison.controller.aliPay.starter.web;

import com.alipay.api.response.*;
import com.tinygray.madison.controller.aliPay.api.PCPagePay;
import com.tinygray.madison.controller.aliPay.starter.domain.PagePay.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author: qlh
 * @Desctiption: pc网站支付宝支付功能
 * @Date: 2020/7/25 11:10
 */
@Controller
@RequestMapping(value = "/alipay/pc/page")
@Api(tags = "ali-pay-trade-pc_website")
public class AliPCPagePayController {

    @Autowired
    private PCPagePay pcPagePay;

    @GetMapping(value = "/pay")
    public void pagePay(@Validated PagePayForm pagePayForm, HttpServletResponse response) {
        AlipayTradePagePayResponse payResponse = pcPagePay.pay(pagePayForm);
        try {
            if (payResponse.isSuccess()) {
                response.setContentType("text/html;charset=UTF-8");
                response.getWriter().write(payResponse.getBody());
                response.getWriter().flush();
                response.getWriter().close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
