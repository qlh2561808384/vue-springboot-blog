package com.tinygray.madison.service;


import java.awt.image.BufferedImage;

/**
 * SysCaptchaService
 *
 * @author madison
 * @date 2020/10/19 18:56
 * @email 2561808384@qq.com
 * @description 验证码类
 */
public interface SysCaptchaService {

    /**
     * 获取验证码
     * @param uuid
     * @return
     */
    BufferedImage getCaptcha(String uuid) throws Exception;

    /**
     * 验证验证码
     * @param uuid
     * @param code
     * @return
     */
    boolean validate(String uuid, String code);
}
