package com.tinygray.madison.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tinygray.madison.entity.TinygrayArticleTag;
import com.tinygray.madison.entity.TinygrayTag;
import com.tinygray.madison.service.SysTinygrayArticleTagService;
import com.tinygray.madison.service.SysTinygrayTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 文章标签表 前端控制器
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
@Api(tags = "标签控制器")
@RestController
@RequestMapping("/tinygray-tag")
public class SysTinygrayTagController extends ApiController {
    @Autowired
    SysTinygrayTagService sysTinygrayTagService;
    @Autowired
    SysTinygrayArticleTagService sysTinygrayArticleTagService;
    @ApiOperation(value = "获取所有文章标签")
    @GetMapping("tagList")
    public R getTagList(Page<TinygrayTag> page, @Param("keyword") String keyword) {
        IPage<TinygrayTag> allTagList = sysTinygrayTagService.getTagList(page, keyword);
        return success(allTagList);
    }

    @PostMapping("saveTag")
    public R saveTag(@RequestBody TinygrayTag tinygrayTag) {
        return success(sysTinygrayTagService.saveTag(tinygrayTag));
    }

    @DeleteMapping("deleteTagByIds")
    public R deleteTagByIds(@RequestBody String[] ids) {
        for (String id : ids) {
            List<TinygrayArticleTag> articleTagList = sysTinygrayArticleTagService.selectArticleIdByTagId(id);
            if (!CollectionUtils.isEmpty(articleTagList)) {
                TinygrayArticleTag tinygrayArticleTag = articleTagList.get(0);
                if (0 != tinygrayArticleTag.getArticleId()) {
                    return R.failed("该标签下有文章，无法删除");
                }
            }
        }
        return success(sysTinygrayTagService.removeByIds(Arrays.asList(ids)));
    }
}
