package com.tinygray.madison.controller.aliPay.api.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.*;
import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.tinygray.madison.controller.aliPay.api.AliPayAssist;
import com.tinygray.madison.controller.aliPay.config.AliPayProperties;
import com.tinygray.madison.controller.aliPay.domain.PublicPay.*;
import com.tinygray.madison.controller.aliPay.enums.ResponseEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Class: AliPayAssistImpl
 * @Description: AliPayAssistImpl$
 * @title: AliPayAssistImpl
 * @Author qlh
 * @Date: 2020/10/30 14:40
 * @Version 1.0
 */
@Service
public class AliPayAssistImpl implements AliPayAssist {
    @Autowired
    AlipayClient alipayClient;
    @Autowired
    AliPayProperties aliPayProperties;

    @Override
    public AlipayTradeQueryResponse query(AliPayQueryDTO queryDTO) {
        AlipayTradeQueryModel model = new AlipayTradeQueryModel();
        BeanUtils.copyProperties(queryDTO, model);
        AlipayTradeQueryRequest alipayRequest = new AlipayTradeQueryRequest();
        alipayRequest.setBizModel(model);
//        alipayRequest.setBizContent(JSON.toJSONString(queryDTO));
        AlipayTradeQueryResponse response;
        try {
            response = alipayClient.execute(alipayRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            response = new AlipayTradeQueryResponse();
            response.setCode(ResponseEnum.PAGE_PAY_QUERY_ERROR.getCode());
            response.setMsg(ResponseEnum.PAGE_PAY_QUERY_ERROR.getMessage());
        }
        return response;
    }

    @Override
    public AlipayTradeRefundResponse refund(RefundDTO refundDTO) {
        AlipayTradeRefundModel model = new AlipayTradeRefundModel();
        BeanUtils.copyProperties(refundDTO, model);
        AlipayTradeRefundRequest alipayRequest = new AlipayTradeRefundRequest();
        alipayRequest.setBizModel(model);
//        alipayRequest.setBizContent(JSON.toJSONString(refundDTO));
        AlipayTradeRefundResponse response;
        try {
            response = alipayClient.execute(alipayRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            response = new  AlipayTradeRefundResponse();
            response.setCode(ResponseEnum.REFUND_ERROR.getCode());
            response.setMsg(ResponseEnum.REFUND_ERROR.getMessage());
        }
        return response;
    }

    @Override
    public AlipayTradeFastpayRefundQueryResponse refundQuery(RefundQueryDTO refundQueryDTO) {
        AlipayTradeFastpayRefundQueryModel model = new AlipayTradeFastpayRefundQueryModel();
        BeanUtils.copyProperties(refundQueryDTO, model);
        AlipayTradeFastpayRefundQueryRequest alipayRequest = new AlipayTradeFastpayRefundQueryRequest();
        alipayRequest.setBizModel(model);
//        alipayRequest.setBizContent(JSON.toJSONString(refundQueryDTO));
        AlipayTradeFastpayRefundQueryResponse response;
        try {
            response = alipayClient.execute(alipayRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            response = new AlipayTradeFastpayRefundQueryResponse();
            response.setCode(ResponseEnum.REFUND_QUERY_ERROR.getCode());
            response.setMsg(ResponseEnum.REFUND_QUERY_ERROR.getMessage());
        }

        return response;
    }

    @Override
    public AlipayTradeCloseResponse close(CloseDTO closeDTO) {
        AlipayTradeCloseModel model = new AlipayTradeCloseModel();
        BeanUtils.copyProperties(closeDTO, model);
        AlipayTradeCloseRequest alipayRequest = new AlipayTradeCloseRequest();
        alipayRequest.setBizModel(model);
//        alipayRequest.setBizContent(JSON.toJSONString(closeDTO));
        AlipayTradeCloseResponse response;
        try {
            response = alipayClient.execute(alipayRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            response = new AlipayTradeCloseResponse();
            response.setCode(ResponseEnum.CLOSE_ERROR.getCode());
            response.setMsg(ResponseEnum.CLOSE_ERROR.getMessage());
        }
        return response;
    }

    @Override
    public AlipayDataDataserviceBillDownloadurlQueryResponse queryBillDownloadUrl(BillDownloadUrlDTO billDownloadUrlDTO) {
        AlipayDataDataserviceBillDownloadurlQueryModel model  = new AlipayDataDataserviceBillDownloadurlQueryModel();
        BeanUtils.copyProperties(billDownloadUrlDTO, model);
        AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
        request.setBizModel(model);
        AlipayDataDataserviceBillDownloadurlQueryResponse response = null;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return response;
    }
}
