package com.tinygray.madison.controller.aliPay.starter.web;

import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.tinygray.madison.controller.aliPay.api.AppPay;
import com.tinygray.madison.controller.aliPay.starter.domain.AppPay.AppPayForm;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Class: AliAPPPayController
 * @Description: app支付宝支付功能
 * @title: AliAPPPayController
 * @Author qlh
 * @Date: 2020/10/29 15:33
 * @Version 1.0
 */
@RestController
@Api(tags = "ali-pay-trade-app")
@RequestMapping("/alipay/app")
public class AliAPPPayController {

    @Autowired
    private AppPay appPay;

    @GetMapping("/pay")
    public ResponseEntity appPay(@Validated AppPayForm appPayForm){
        AlipayTradeAppPayResponse payResponse = appPay.pay(appPayForm);
        return ResponseEntity.ok().body(payResponse.getBody());
    }
}
