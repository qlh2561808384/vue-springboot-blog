package com.tinygray.madison.service.serviceImpl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tinygray.madison.entity.TinygrayTag;
import com.tinygray.madison.mapper.SysTinygrayTagMapper;
import com.tinygray.madison.service.SysTinygrayTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章标签表 服务实现类
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
@Service
public class SysTinygrayTagServiceImpl extends ServiceImpl<SysTinygrayTagMapper, TinygrayTag> implements SysTinygrayTagService {

    @Autowired
    SysTinygrayTagMapper sysTinygrayTagMapper;
    @Override
    public IPage<TinygrayTag> getTagList(Page<TinygrayTag> page, String keyword) {
        LambdaQueryWrapper<TinygrayTag> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (!StrUtil.isEmpty(keyword)) {
            lambdaQueryWrapper.like(TinygrayTag::getName, keyword);
            lambdaQueryWrapper.or();
            lambdaQueryWrapper.eq(TinygrayTag::getName, keyword);
        }
        return sysTinygrayTagMapper.selectPage(page, lambdaQueryWrapper);
    }

    @Override
    public boolean saveTag(TinygrayTag tinygrayTag) {
        if (tinygrayTag != null) {
            if (0 == tinygrayTag.getId()) {
                return this.save(tinygrayTag);
            }else {
                return this.saveOrUpdate(tinygrayTag);
            }
        }else {
            return false;
        }
    }
}
