package com.tinygray.madison.vo;

import lombok.Data;

/**
 * @Class: SysRoleAndPermissionVo
 * @Description: SysRoleAndPermissionVo$
 * @title: SysRoleAndPermissionVo
 * @Author qlh
 * @Date: 2020/9/24 15:56
 * @Version 1.0
 */
@Data
public class SysRoleAndPermissionVo {
    Integer  id;
    String  name;
    Integer  pid;
    Integer  roleId;
}
