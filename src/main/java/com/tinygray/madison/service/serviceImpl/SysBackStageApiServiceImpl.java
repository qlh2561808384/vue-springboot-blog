package com.tinygray.madison.service.serviceImpl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tinygray.madison.entity.TinygrayBackstageApi;
import com.tinygray.madison.mapper.SysBackStageApiMapper;
import com.tinygray.madison.service.SysBackStageApiService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysBackStageApiServiceImpl extends ServiceImpl<SysBackStageApiMapper, TinygrayBackstageApi> implements SysBackStageApiService {
    @Override
    public List<TinygrayBackstageApi> getApiUrlByUserName(String username) {
        return this.baseMapper.getApiUrlByUserName(username);
    }
}
