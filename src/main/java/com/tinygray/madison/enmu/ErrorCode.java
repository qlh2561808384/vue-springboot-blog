package com.tinygray.madison.enmu;

import com.baomidou.mybatisplus.extension.api.IErrorCode;

public enum ErrorCode implements IErrorCode {
    FAILED(-1L, "操作失败"),
    SUCCESS(0L, "执行成功"),

    LOGIN_ACCOUNT_DOES_NOT_EXIST(10000, "登录失败:账号不存在，请重新尝试！"),
    LOGIN_WRONG_PASSWORD(10001, "登录失败:密码不正确！"),
    LOGIN_VERIFICATION_CODE_ERROR(10011, "登录失败:验证码错误！"),

    REGISTER_USER_ALREADY_EXISTS(10002, "这个用户已经存在，不能重复。"),
    REGISTER_EMAIL_REGISTERED(10003, "该邮箱已经被注册，不能重复。"),
    REGISTER_USER_OBJECT_IS_EMPTY(10004, "错误消息：用户对象为空！"),
    REGISTER_FAILED_TO_SEND_CODE(10012, "服务怠机,请稍后重试"),

    EMAIL_ACCOUNT_DOES_NOT_EXIST(10005, "Account does not exist"),
    EMAIL_RETRIEVE_PASSWORD(10006, "Email retrieve password"),
    EMAIL_TIME_HAS_EXPIRED(10007, "Email time has expired"),
    EMAIL_SID_IS_INCOMPLETE_CONTENT(10008, "sid is incomplete content"),
    EMAIL_SID_IS_ERROR(10009, "sid is error"),
    EMAIL_NO_ERROR(10010, "no error"),;

    private final long code;
    private final String msg;

    private ErrorCode(final long code, final String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String toString() {
        return String.format("{\"code\": %s, \"msg\":\"%s\"}", this.code, this.msg);
    }

    @Override
    public long getCode() {
        return 0;
    }

    @Override
    public String getMsg() {
        return null;
    }
}
