package com.tinygray.madison.controller.aliPay.starter.domain.PagePay;

import com.sun.istack.internal.NotNull;
import com.tinygray.madison.controller.aliPay.domain.PagePay.PagePayDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: qlh
 * @Desctiption:
 * @Date: 2020/7/25 11:19
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PagePayForm extends PagePayDTO {

    @NotNull
    @Override
    public String getTotalAmount() {
        return super.getTotalAmount();
    }
}
