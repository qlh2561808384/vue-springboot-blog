package com.tinygray.madison.service;

import com.tinygray.madison.entity.TinygrayArticleTag;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 文章标签关系表 服务类
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
public interface SysTinygrayArticleTagService extends IService<TinygrayArticleTag> {

    List<TinygrayArticleTag> selectArticleIdByTagId(String id);
}
