package com.tinygray.madison.dto;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Class: SmsDto
 * @Description: SmsDto$
 * @title: SmsDto
 * @Author qlh
 * @Date: 2020/9/10 14:25
 * @Version 1.0
 */
@Data
@Component
@ApiModel(description = "短信业务dto")
public class SmsDto {
    @ApiModelProperty(value = "访问阿里云API的验证")
    @Value("${sms.accessKeyId}")
    private String accessKeyId;

    @ApiModelProperty(value = "访问阿里云API的验证")
    @Value("${sms.accessKeySecret}")
    private String accessKeySecret;

    @ApiModelProperty(value = "阿里云云通信短信API产品")
    @Value("${sms.product}")
    private String product;

    @ApiModelProperty(value = "产品域名")
    @Value("${sms.domain}")
    private String domain;

    @ApiModelProperty(value = "签名")
    @Value("${sms.signName}")
    private String signName;

    @ApiModelProperty(value = "发送验证码模版CODE")
    @Value("${sms.templateCode}")
    private String templateCode;

    @ApiModelProperty(value = "替换阿里云短信模板中的变量；比如：亲爱的${name},您的验证码为${code}")
    private String templateParam;
}
