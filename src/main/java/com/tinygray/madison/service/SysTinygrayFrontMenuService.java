package com.tinygray.madison.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;

import java.util.List;

/**
 * <p>
 * 前端菜单表 服务类
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
public interface SysTinygrayFrontMenuService extends IService<TinygrayFrontMenu> {
    List<TinygrayFrontMenu> getMenusByUserName(String username);

    List<TinygrayFrontMenu> selectMenus();

    TinygrayFrontMenu getMenuById(Integer menuId);

    boolean deleteMenuById(TinygrayFrontMenuVo tinygrayFrontMenuVo);
}
