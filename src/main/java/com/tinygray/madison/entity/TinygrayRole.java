package com.tinygray.madison.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TinygrayRole implements Serializable {

  private long id;
  private String rolename;
  private Long status;
  private String description;
}
