package com.tinygray.madison.enmu;

import lombok.Getter;

@Getter
public enum Constants {
    STATE(1),;
    private final int state;
    private Constants(int state){
        this.state = state;
    }
}
