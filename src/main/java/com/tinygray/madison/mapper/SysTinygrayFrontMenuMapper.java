package com.tinygray.madison.mapper;

import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tinygray.madison.providers.BackStageApiProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

/**
 * <p>
 * 前端菜单表 Mapper 接口
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
public interface SysTinygrayFrontMenuMapper extends BaseMapper<TinygrayFrontMenu> {
    @SelectProvider(type = BackStageApiProvider.class,method = "getMenusByUserName")
    List<TinygrayFrontMenu> getMenusByUserName(@Param("username") String username);
}
