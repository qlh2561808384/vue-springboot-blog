package com.tinygray.madison.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@Api(tags = "TestController测试")
@RestController
public class TestController {

    @ApiOperation("首页api")
    @GetMapping("/")
    public String login(String... index) {
        return "Hello login ~";
    }

    @ApiOperation("helloWord Api")
    @GetMapping("/index")
    public String index() {
        return "Hello World ~";
    }

    @ApiOperation("admin Api")
    @GetMapping("/admin/hello")
    public String admin() {
        return "hello admin!";
    }

    @ApiOperation("user Api")
    @GetMapping("/user/hello")
    public String user() {
        return "hello user";
    }
}
