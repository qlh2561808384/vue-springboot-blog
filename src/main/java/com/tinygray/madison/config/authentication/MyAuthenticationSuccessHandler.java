package com.tinygray.madison.config.authentication;

import com.baomidou.mybatisplus.extension.api.R;
import com.tinygray.madison.components.JwtTokenUtil;
import com.tinygray.madison.components.TokenCache;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.service.SysTinygrayFrontMenuService;
import com.tinygray.madison.service.SysUserService;
import com.tinygray.madison.service.redis.RedisService;
import com.tinygray.madison.util.MenuTreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登录成功操作
 */
@Component
public class MyAuthenticationSuccessHandler extends JSONAuthentication implements AuthenticationSuccessHandler {

    @Autowired
    SysUserService sysUserService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    SysTinygrayFrontMenuService sysTinygrayFrontMenuService;

    @Autowired
    MenuTreeUtil menuTreeUtil;

    @Autowired
    RedisService redisService;

    // 过期时间 毫秒,设置默认1周的时间过期
//    private static final long EXPIRATION_TIME = 3600L * 24 * 7;
    private static final long EXPIRATION_TIME = 60L;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        //取得账号信息
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //
        System.out.println("userDetails = " + userDetails);
        //取token
        //好的解决方案，登录成功后token存储到数据库中
        //只要token还在过期内，不需要每次重新生成
        //先去缓存中找
//        String redisToken = redisService.get(userDetails.getUsername()).toString();
        String token = TokenCache.getTokenFromCache(userDetails.getUsername());
        if(token ==null) {
            System.out.println("初次登录，token还没有，生成新token。。。。。。");
            //如果token为空，则去创建一个新的token
//            jwtTokenUtil = new JwtTokenUtil();
            token = jwtTokenUtil.generateToken(userDetails);
            //把新的token存储到缓存中
//            redisService.setEx(userDetails.getUsername(), token, EXPIRATION_TIME);
            TokenCache.setToken(userDetails.getUsername(),token);
        }

        /**
         * 获取前端需要资源，根据用户名获取前端菜单、等资源
         */
        //TODO。。。
        List<TinygrayFrontMenu> tinygrayFrontMenus = sysTinygrayFrontMenuService.getMenusByUserName(userDetails.getUsername());
        List<Object> menus = menuTreeUtil.menuList(tinygrayFrontMenus);
//        List<SysFrontendMenuTable> menus = service.getMenusByUserName(userDetails.getUsername());      //加载前端菜单

        //
        Map<String,Object> map = new HashMap<>();
        map.put("username",userDetails.getUsername());
        map.put("auth",userDetails.getAuthorities());
        map.put("menus",menus);
        map.put("token",token);
        //装入token
        R<Map<String,Object>> data = R.ok(map);
        //输出
        this.WriteJSON(request, response, data);

    }
}
