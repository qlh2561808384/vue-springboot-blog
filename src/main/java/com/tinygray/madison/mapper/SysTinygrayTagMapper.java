package com.tinygray.madison.mapper;

import com.tinygray.madison.entity.TinygrayTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文章标签表 Mapper 接口
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
public interface SysTinygrayTagMapper extends BaseMapper<TinygrayTag> {

}
