package com.tinygray.madison.controller.aliPay.domain.PublicPay;

import com.alibaba.fastjson.annotation.JSONField;
import com.alipay.api.internal.mapping.ApiField;
import lombok.Data;

/**
 * @Class: BillDownloadUrlDTO
 * @Description: BillDownloadUrlDTO$
 * @title: BillDownloadUrlDTO
 * @Author qlh
 * @Date: 2020/10/28 16:45
 * @Version 1.0
 */
@Data
public class BillDownloadUrlDTO {
    /**
     * 账单时间：日账单格式为yyyy-MM-dd，最早可下载2016年1月1日开始的日账单；月账单格式为yyyy-MM，最早可下载2016年1月开始的月账单。不支持下载当日或者当月账单
     */
    @JSONField(name = "bill_date")
    private String billDate;

    /**
     * 账单类型，商户通过接口或商户经开放平台授权后其所属服务商通过接口可以获取以下账单类型：trade、signcustomer；trade指商户基于支付宝交易收单的业务账单；signcustomer是指基于商户支付宝余额收入及支出等资金变动的帐务账单。
     */
    @JSONField(name = "bill_type")
    private String billType;
}
