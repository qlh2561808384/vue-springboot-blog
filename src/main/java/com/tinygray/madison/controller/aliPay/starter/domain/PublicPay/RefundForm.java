package com.tinygray.madison.controller.aliPay.starter.domain.PublicPay;

import com.sun.istack.internal.NotNull;
import com.tinygray.madison.controller.aliPay.domain.PublicPay.RefundDTO;

/**
 * @Author: qlh
 * @Desctiption:
 * @Date: 2020/7/25 11:19
 */
public class RefundForm extends RefundDTO {

    @NotNull
    @Override
    public String getRefundAmount() {
        return super.getRefundAmount();
    }
}

