package com.tinygray.madison.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TinygrayPermission implements Serializable {

  private String permissionName;
  private String permissionLable;
  private long id;
}
