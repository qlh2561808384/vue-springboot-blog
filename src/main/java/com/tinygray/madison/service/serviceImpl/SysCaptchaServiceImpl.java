package com.tinygray.madison.service.serviceImpl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.tinygray.madison.enmu.ErrorCode;
import com.tinygray.madison.service.SysCaptchaService;
import com.tinygray.madison.service.redis.RedisKeyConstants;
import com.tinygray.madison.service.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.code.kaptcha.Producer;
import java.awt.image.BufferedImage;

/**
 * SysCaptchaServiceImpl
 *
 * @author madison
 * @date 2020/10/19 18:56
 * @email 2561808384@qq.com
 * @description
 */
@Service
public class SysCaptchaServiceImpl implements SysCaptchaService {
    @Autowired
    private Producer producer;

    @Autowired
    RedisService redisService;

    /**  验证码过期时长5秒 */
    private final static long CAPTCHA_EXPIRE = 60 * 5;
    /**
     * 获取验证码
     *
     * @param uuid
     * @return
     */
    @Override
    public BufferedImage getCaptcha(String uuid) throws Exception{
        if(StringUtils.isBlank(uuid)){
            throw new Exception(ErrorCode.LOGIN_VERIFICATION_CODE_ERROR.toString());
        }
        //生成文字验证码
        String code = producer.createText();
        // 存进redis,5分钟后过期
        redisService.setEx(genRedisKey(uuid), code, CAPTCHA_EXPIRE);
        return producer.createImage(code);
    }

    @Override
    public boolean validate(String uuid, String code) {
        if (StringUtils.isBlank(uuid) || StringUtils.isBlank(code)) {
            return false;
        }
        // 从redis中取
        String redisKey = genRedisKey(uuid);
        String captchaCode = redisService.get(redisKey).toString();
        //删除验证码
        redisService.remove(redisKey);
        if (code.equalsIgnoreCase(captchaCode)) {
            return true;
        }
        return false;
    }

    private String genRedisKey(String uuid) {
        return RedisKeyConstants.MANAGE_SYS_CAPTCHA + uuid;
    }
}
