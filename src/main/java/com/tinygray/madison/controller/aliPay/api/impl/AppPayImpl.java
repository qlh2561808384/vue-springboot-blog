package com.tinygray.madison.controller.aliPay.api.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.tinygray.madison.controller.aliPay.api.AppPay;
import com.tinygray.madison.controller.aliPay.config.AliPayProperties;
import com.tinygray.madison.controller.aliPay.domain.AppPay.AppPayDTO;
import com.tinygray.madison.controller.aliPay.enums.ResponseEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Class: AppPayImpl
 * @Description: AppPayImpl$
 * @title: AppPayImpl
 * @Author qlh
 * @Date: 2020/10/29 15:37
 * @Version 1.0
 */
@Slf4j
@Service
public class AppPayImpl implements AppPay {

    /*  private AlipayClient alipayClient;

      private String notifyUrl;

      private String returnUrl;*/
    @Autowired
    AlipayClient alipayClient;
    @Autowired
    AliPayProperties aliPayProperties;

    @Override
    public AlipayTradeAppPayResponse pay(AppPayDTO appPayDTO) {
        appPayDTO.setBody("支付宝沙箱app环境测试");
        appPayDTO.setProductCode("QUICK_MSECURITY_PAY");
        appPayDTO.setTimeoutExpress("1h");
        appPayDTO.setGoodsType("0");
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        BeanUtils.copyProperties(appPayDTO,model);
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        request.setBizModel(model);
        request.setNotifyUrl(aliPayProperties.getNotifyUrl());
        AlipayTradeAppPayResponse response;
        try {
            response = alipayClient.sdkExecute(request);
        } catch (AlipayApiException e) {
            response = new AlipayTradeAppPayResponse();
            response.setCode(ResponseEnum.PAGE_PAY_ERROR.getCode());
            response.setMsg(ResponseEnum.PAGE_PAY_ERROR.getMessage());
        }
        return response;
    }

  /*  public AlipayClient getAlipayClient() {
        return alipayClient;
    }

    public void setAlipayClient(AlipayClient alipayClient) {
        this.alipayClient = alipayClient;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }*/
}
