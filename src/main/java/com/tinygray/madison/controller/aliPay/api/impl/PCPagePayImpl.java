package com.tinygray.madison.controller.aliPay.api.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.*;
import com.alipay.api.request.*;
import com.alipay.api.response.*;
import com.tinygray.madison.controller.aliPay.api.PCPagePay;
import com.tinygray.madison.controller.aliPay.config.AliPayProperties;
import com.tinygray.madison.controller.aliPay.domain.PagePay.*;
import com.tinygray.madison.controller.aliPay.enums.ResponseEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: qlh
 * @Desctiption: 支付宝PC页面支付接口实现类
 * @Date: 2020/7/24 21:14
 */
@Slf4j
@Service
public class PCPagePayImpl implements PCPagePay {

    /* private AlipayClient alipayClient;

     private String notifyUrl;

     private String returnUrl;*/
    @Autowired
    AlipayClient alipayClient;
    @Autowired
    AliPayProperties aliPayProperties;

    @Override
    public AlipayTradePagePayResponse pay(PagePayDTO pagePayDTO) {
        pagePayDTO.setGoodsType("0");
        pagePayDTO.setBody("支付宝沙箱环境测试");
        AlipayTradePagePayModel model = new AlipayTradePagePayModel();
        BeanUtils.copyProperties(pagePayDTO, model);
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setNotifyUrl(aliPayProperties.getNotifyUrl());
        alipayRequest.setReturnUrl(aliPayProperties.getReturnUrl());
        alipayRequest.setBizModel(model);
//        alipayRequest.setBizContent(JSON.toJSONString(pagePayDTO));
        AlipayTradePagePayResponse response;
        try {
            response = alipayClient.pageExecute(alipayRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            response = new AlipayTradePagePayResponse();
            response.setCode(ResponseEnum.PAGE_PAY_ERROR.getCode());
            response.setMsg(ResponseEnum.PAGE_PAY_ERROR.getMessage());
        }
        return response;
    }

   /* public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public void setAlipayClient(AlipayClient alipayClient) {
        this.alipayClient = alipayClient;
    }*/
}
