package com.tinygray.madison.service.redis;

/**
 * RedisKeyConstants
 *
 * @author madison
 * @date 2020/10/20 13:44
 * @email 2561808384@qq.com
 * @description redis baseKey管理常量
 */
public class RedisKeyConstants {



    /**
     * 后台管理验证码key
     */
    public final static String MANAGE_SYS_CAPTCHA = "MANAGE:SYS:CAPTCHA:";

    /**
     * 手机号注册 验证码 key
     */
    public final static String REGISTER_PHONE_VERIFICATION_CODE = "REGISTER:SYS:USER:PHONE:VERIFICATION_CODE:";



}
