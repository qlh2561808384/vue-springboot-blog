package com.tinygray.madison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tinygray.madison.entity.TinygrayBackstageApi;

import java.util.List;

public interface SysBackStageApiService extends IService<TinygrayBackstageApi> {
    /**
     * 根据用户名称查询API接口URL
     * @param username
     * @return
     */
    List<TinygrayBackstageApi> getApiUrlByUserName(String username);
}
