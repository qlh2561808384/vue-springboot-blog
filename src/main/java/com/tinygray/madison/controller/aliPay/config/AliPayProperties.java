package com.tinygray.madison.controller.aliPay.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @Author: qlh
 * @Desctiption: 国内支付宝支付配置类（电脑网站）
 * @Date: 2020/7/16 11:23
 */
@Data
@Slf4j
@Component
@ConfigurationProperties("alipay")
public class AliPayProperties {

    /**
     * 商户应用id
     * 收款账号既是您的APPID对应支付宝账号  APPID 即创建应用后生成
     */
    public  String appId ;

    /**
     * RSA私钥，用于对商户请求报文加签
     * 商户私钥，您的PKCS8格式RSA2私钥 开发者私钥，由开发者自己生成
     */
    public  String merchantPrivateKey;

    /**
     * 支付宝RSA公钥，用于验签支付宝应答
     * 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
     */
    public  String alipayPublicKey;

    /**
     * 同步地址
     * 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    public  String notifyUrl ;

    /**
     * 异步地址
     * 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    public  String returnUrl;

    /**
     * 签名类型
     * 签名方式 商户生成签名字符串所使用的签名算法类型，目前支持 RSA2 和 RSA，推荐使用 RSA2
     */
    public  String signType = "RSA2";

    /**
     * 格式
     */
    private String formate = "json";

    /**
     * 编码集，支持 GBK/UTF-8
     */
    public  String charset = "utf-8";

    /**
     * 支付宝gatewayUrl
     */
    public  String gatewayUrl;

    /**
     *  @Bean:
     *      用于方法上，该方法返回一个实例对象告诉Spring，然后在Spring容器中注册成一个bean，通常方法体中包含了最终产生bean实例的逻辑。
     *  主要用于第三方库中的类需要装配到Spring容器，因为无法在第三方库中加@Component注解，只能通过@Bean来实现。
     *
     */
    @Bean
    public AlipayClient alipayClient(){
        return new DefaultAlipayClient(this.getGatewayUrl(),
                this.getAppId(),
                this.getMerchantPrivateKey(),
                this.getFormate(),
                this.getCharset(),
                this.getAlipayPublicKey(),
                this.getSignType());
    }

}
