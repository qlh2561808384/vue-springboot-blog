package com.tinygray.madison.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TinygrayRoleFrontMenu {

  private long id;
  private long roleid;
  private long frontMenuId;

}
