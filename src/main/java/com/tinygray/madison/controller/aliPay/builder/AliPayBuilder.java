package com.tinygray.madison.controller.aliPay.builder;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.tinygray.madison.controller.aliPay.api.AppPay;
import com.tinygray.madison.controller.aliPay.api.PCPagePay;
import com.tinygray.madison.controller.aliPay.api.impl.AppPayImpl;
import com.tinygray.madison.controller.aliPay.api.impl.PCPagePayImpl;
import com.tinygray.madison.controller.aliPay.config.AliPayProperties;
import com.tinygray.madison.controller.aliPay.util.AliPaySignUtil;
import lombok.Setter;

import java.util.Objects;

/**
 * @Author: qlh
 * @Desctiption:
 * @Date: 2018/7/25 10:22
 */
/*
public class AliPayBuilder {


    private static AliPayBuilder aliPayBuilder;

    @Setter
    private AlipayClient alipayClient;

    @Setter
    private AliPayProperties properties;

    public static final String DEFAULT_FORMAT = "json";

    private AliPayBuilder(){}

    public static final AliPayBuilder builder(AliPayProperties properties){
        Objects.requireNonNull(properties," properties must be not null");
        if(aliPayBuilder == null){
            aliPayBuilder = new AliPayBuilder();
        }
        AlipayClient alipayClient = new DefaultAlipayClient(
                properties.getGatewayUrl(),
                properties.getAppId(),
                properties.getMerchantPrivateKey(),
                DEFAULT_FORMAT,properties.getCharset(),
                properties.getAlipayPublicKey(),properties.getSignType());
        aliPayBuilder.setAlipayClient(alipayClient);
        aliPayBuilder.setProperties(properties);
        return aliPayBuilder;
    }

    public PCPagePay preparePcPagePay(){
        PCPagePayImpl pcPagePay = new PCPagePayImpl();
        pcPagePay.setAlipayClient(this.alipayClient);
        pcPagePay.setNotifyUrl(this.properties.getNotifyUrl());
        pcPagePay.setReturnUrl(this.properties.getReturnUrl());
        return pcPagePay;
    }

    public AppPay prepareAppPay(){
        AppPayImpl APPPay = new AppPayImpl();
        APPPay.setAlipayClient(this.alipayClient);
        APPPay.setNotifyUrl(this.properties.getNotifyUrl());
        APPPay.setReturnUrl(this.properties.getReturnUrl());
        return APPPay;
    }

    public AliPaySignUtil prepareSignUtil(){
        AliPaySignUtil paySignUtil = new AliPaySignUtil();
        paySignUtil.setPayProperties(this.properties);
        return paySignUtil;
    }



}
*/
