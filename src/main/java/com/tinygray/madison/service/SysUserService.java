package com.tinygray.madison.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tinygray.madison.entity.TinygrayUser;

public interface SysUserService extends IService<TinygrayUser> {

    TinygrayUser getUserByUserName(String username, String email);

    boolean register(TinygrayUser tinygrayUser) throws Exception;

    boolean checkLogin(String username,String password) throws Exception;

    R findUserByMail(String basePath, String email);

    R verifyMail(String sid, String account);

    IPage<TinygrayUser> getAllUser(Page<TinygrayUser> page, String keyword);

    boolean delUser(String id);
}
