package com.tinygray.madison.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tinygray.madison.entity.TinygrayArticleTag;
import com.tinygray.madison.mapper.SysTinygrayArticleTagMapper;
import com.tinygray.madison.service.SysTinygrayArticleTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 文章标签关系表 服务实现类
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
@Service
public class SysTinygrayArticleTagServiceImpl extends ServiceImpl<SysTinygrayArticleTagMapper, TinygrayArticleTag> implements SysTinygrayArticleTagService {
    @Autowired
    SysTinygrayArticleTagMapper sysTinygrayArticleTagMapper;
    @Override
    public List<TinygrayArticleTag> selectArticleIdByTagId(String id) {
        LambdaQueryWrapper<TinygrayArticleTag> lambdaQueryWrapper = new LambdaQueryWrapper<TinygrayArticleTag>();
        lambdaQueryWrapper.eq(TinygrayArticleTag::getTagId, id);
        return this.sysTinygrayArticleTagMapper.selectList(lambdaQueryWrapper);
    }
}
