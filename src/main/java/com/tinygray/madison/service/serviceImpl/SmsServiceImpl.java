package com.tinygray.madison.service.serviceImpl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.tinygray.madison.dto.SmsDto;
import com.tinygray.madison.service.SmsService;
import com.tinygray.madison.service.redis.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @Class: SmsServiceImpl
 * @Description: SmsServiceImpl$
 * @title: SmsServiceImpl
 * @Author qlh
 * @Date: 2020/9/10 14:38
 * @Version 1.0
 */
@Service
public class SmsServiceImpl implements SmsService {
    @Autowired
    SmsDto smsDto;

    @Override
    public boolean sendSms(String phoneNumber, String code) {
        /**
         * 设置短信模板的值
         */
        JSONObject object = new JSONObject();
        try {
            object.put("code",code);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        smsDto.setTemplateParam(object.toString());
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", smsDto.getAccessKeyId(), smsDto.getAccessKeySecret());
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain(smsDto.getDomain());
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", phoneNumber);
        request.putQueryParameter("SignName", smsDto.getSignName());
        request.putQueryParameter("TemplateCode", smsDto.getTemplateCode());
        request.putQueryParameter("TemplateParam", smsDto.getTemplateParam());
        try {
            /**
             * 请求失败这里会抛ClientException异常
             */
            CommonResponse response = client.getCommonResponse(request);
            System.out.println("阿里云短信服务返回消息:" + response.getData());
            /**
             * 使用alibaba的fastjson
             */
            Map<String, Object> map = JSON.parseObject(response.getData());
            return ("OK").equals(map.get("Code"));
        } catch (ClientException e) {
            e.printStackTrace();
            System.out.println("发送短信错误，错误信息:" + e.getMessage());
        }
        return false;
    }
}
