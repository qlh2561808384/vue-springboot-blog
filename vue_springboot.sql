#创建数据库
create database tinygray;

/*
    用户基础信息表 users
    id
    nickname
    avatar

    用户授权信息表 user_auths
    id
    user_id
    identity_type 登录类型（手机号 邮箱 用户名）或第三方应用名称（微信 微博等）
    identifier 标识（手机号 邮箱 用户名或第三方应用的唯一标识）
    credential 密码凭证（站内的保存密码，站外的不保存或保存token）
*/

#用户基础信息表
create table users
(
    id int auto_increment comment '用户基础信息表id',
    nickname varchar(50) not null comment '用户昵称',
    avatar varchar(100) not null comment '用户头像',
    constraint table_name_pk
        primary key (id)
);
#用户授权信息表
create table user_auths
(
    id int auto_increment comment '用户授权信息表id',
    user_id int not null comment '用户基础信息表id',
    identity_type varchar(20) not null comment '登录类型（手机号 邮箱 用户名）或第三方应用名称（微信 微博等）',
    identifier varchar(100) not null comment '标识（手机号 邮箱 用户名或第三方应用的唯一标识）',
    credential varchar(100) not null comment '密码凭证（站内的保存密码，站外的不保存或保存token）',
    constraint user_auths_pk
        primary key (id)
);


# 测试用户，暂时不用上面的复杂的用户，后期再改。
create table tinygray_user
(
    id int auto_increment comment '测试用户id',
    nickname varchar(50) not null comment '用户昵称',
    password varchar(100) not null comment '用户登陆密码',
    email varchar(50) not null comment '用户邮箱，用于找回密码',
    status tinyint(1) default 0 not null comment '用户状态:默认0。0表示可用，1表示禁用',
    create_time datetime not null comment '用户注册时间',
    constraint test_user_pk
        primary key (id)
)
comment '用户表';
# 测试角色
create table tinygray_role
(
    id int auto_increment comment '角色id',
    rolename varchar(50) not null comment '角色名称',
    status tinyint(1) default 0 not null comment '角色状态：0表示该角色可用，1表示该角色不可用',
    description varchar(50) not null comment '角色描述',
    avatar varchar(255) null comment '用户头像',
    birthday datetime null comment '用户生日',
    age tinyint null comment '用户年龄',
    phone int null comment '用户手机号',
    constraint test_role_pk
        primary key (id)
)
comment '角色表';

# 用户角色关联表
create table tinygray_user_role
(
    id int auto_increment comment '用户角色关系id',
    userid int not null comment '用户id',
    roleid int not null comment '角色id',
    constraint tinygray_user_role_pk
        primary key (id)
)
    comment '用户角色关联表';

#后端api表
create table tinygray_backstage_api
(
    id int auto_increment comment '后端api表 主键id',
    backstage_api_name varchar(50) not null comment '后端api 名称',
    backstage_api_url varchar(50) not null comment '后端api url',
    backstage_api_method varchar(50) null comment '后端api 方法类型',
    description varchar(50) null comment '后端api描述',
    constraint tinygray_backstage_api_pk
        primary key (id)
)
    comment '后端api表';
#角色跟后端api 关系表
create table tinygray_role_backstage_api
(
    id int auto_increment comment '角色跟后端api 关系表 主键',
    roleid int not null comment '角色id',
    backstage_api_id int not null comment '后端api 主键id',
    constraint tinygray_role_backstage_api_pk
        primary key (id)
)
    comment '角色跟后端api 关系表';

#前端菜单表
create table tinygray_front_menu
(
    id int auto_increment comment '前端菜单表id',
    front_menu_name varchar(50) not null comment '前端菜单名称name',
    front_menu_url varchar(50) null comment '前端菜单路径url',
    pid int null comment '树形结构pid => 一级菜单、二级菜单、三级菜单',
    description varchar(50) null comment '菜单描述',
    icon varchar(50) not null comment '菜单图标icon',
    front_menu_sort int not null,
    path varchar(25) null comment '对应前端路由path',
    type tinyint null comment '类型：0表示目录1表示菜单',
    constraint tinygray_front_menu_pk
        primary key (id)
)
    comment '前端菜单表';
#角色跟前端菜单关系表
create table tinygray_role_front_menu
(
    id int auto_increment comment '关系表id',
    roleid int not null comment '角色id',
    front_menu_id int not null comment '前端菜单id',
    constraint tinygray_role_front_menu_pk
        primary key (id)
)
    comment '前端菜单跟角色关系表';
#通过邮箱找密码表
create table tinygray_mail_retrieve
(
    id int auto_increment comment '主键id',
    account varchar(20) not null comment '账号名称',
    sid varchar(50) not null comment '用户账号（account）+过期时间（outtime）+随机值（key）通过md5将这些元素散列加密得到',
    out_time bigint not null comment '过期时间',
    constraint tinygray_mail_retrieve_pk
        primary key (id)
)
    comment '通过邮箱找密码表';

create table tinygray_article
(
    id int auto_increment comment '主键'
        primary key,
    title varchar(50) not null comment '文章标题',
    description text null comment '文章描述',
    author varchar(50) null comment '文章作者',
    content longtext null comment '文章内容',
    content_format longtext null comment 'html的content',
    read_num int default '0' null comment '阅读量',
    comment_num int default '0' null comment '评论量',
    like_num int default '0' null comment '点赞量',
    cover_type int null comment '文章展示类别,1:普通，2：大图片，3：无图片',
    cover text null comment '封面',
    create_time timestamp default CURRENT_TIMESTAMP not null comment '创建时间',
    update_time timestamp default CURRENT_TIMESTAMP not null comment '更新时间',
    recommend tinyint(1) default '0' not null comment '是否推荐文章',
    category_id varchar(50) null comment '分类类别存在多级分类，用逗号隔开',
    publish tinyint default '0' null comment '发布状态',
    top tinyint(1) default '0' null comment '是否置顶'
)
    comment '文章'
;
create table tinygray_tag
(
    id int auto_increment comment '主键',
    name varchar(50) null comment '标签名称',
    constraint tinygray_tag_pk
        primary key (id)
)
    comment '文章标签表';

create table tinygray_article_tag
(
    id int auto_increment,
    article_id int null,
    tag_id int null,
    constraint tinygray_article_tag_pk
        primary key (id)
)
    comment '文章标签关系表';

create table tinygray_sys_param
(
    id int auto_increment,
    param_key int null,
    param_value varchar(255) null,
    menu_url varchar(255) null,
    param_type tinyint null,
    constraint tinygray_sys_param_pk
        primary key (id)
)
    comment '系统参数';
create table tinygray_category
(
    id int auto_increment,
    name varchar(100) null comment '分类名称',
    type tinyint(1) null comment '类别：0文章，1阅读',
    `rank` tinyint(1) null comment '分类级别 一二三级别',
    parent_id int default 0 null comment '父主键',
    constraint tinygray_category_pk
        primary key (id)
)
    comment '分类表';




