package com.tinygray.madison.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.IErrorCode;
import com.baomidou.mybatisplus.extension.api.R;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.entity.TinygrayRole;
import com.tinygray.madison.service.SysRoleService;
import com.tinygray.madison.util.MenuTreeUtil;
import com.tinygray.madison.vo.SysRoleAndPermissionVo;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(tags = "用户角色控制器")
@RestController
@RequestMapping("role")
public class SysRoleController extends ApiController {
    @Autowired
    SysRoleService sysRoleService;
    @Autowired
    MenuTreeUtil menuTreeUtil;
    @ApiOperation(value = "获取角色列表")
    @GetMapping("getAllRole")
    public R getAllRole(@RequestParam("page") int page, @RequestParam("limit") int limit, @RequestParam("keyword") String keyword) {
        PageHelper.startPage(page, limit);
        PageInfo<TinygrayRole> RolePageInfo = new PageInfo<>(sysRoleService.getAllRole(keyword));
        return success(JSONUtil.createObj()
                .putOnce("total", RolePageInfo.getTotal())
                .putOnce("data", RolePageInfo.getList()));
    }

    @ApiOperation(value = "新增用户角色")
    @PostMapping("addRole")
    public R addRole(@RequestBody TinygrayRole tinygrayRole) {
        boolean flag = sysRoleService.addRole(tinygrayRole);
        if (flag) {
            return success(true);
        } else {
            return R.failed(new IErrorCode() {
                @Override
                public long getCode() {
                    return -1;
                }

                @Override
                public String getMsg() {
                    return "角色名称或描述不能为空";
                }
            });
        }
    }

    @ApiOperation(value = "删除用户角色")
    @GetMapping("delRole/{id}")
    public R delRole(@PathVariable(name = "id") String id) {
        return success(sysRoleService.delRole(Long.parseLong(id)));
    }

    @ApiOperation(value = "角色对菜单接口")
    @GetMapping("roleToMenu/{roleId}")
    public R getRoleToMenuList(@PathVariable Integer roleId) {
        List<TinygrayFrontMenu> roleToMenuList = sysRoleService.getRoleToMenuList(roleId);
        List<TinygrayFrontMenuVo> tinygrayFrontMenuVos = menuTreeUtil.selectMenusListForTree(roleToMenuList);
        return success(tinygrayFrontMenuVos);
    }
}
