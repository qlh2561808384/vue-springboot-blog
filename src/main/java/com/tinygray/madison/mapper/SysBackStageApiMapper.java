package com.tinygray.madison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tinygray.madison.entity.TinygrayBackstageApi;
import com.tinygray.madison.providers.BackStageApiProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface SysBackStageApiMapper extends BaseMapper<TinygrayBackstageApi> {
    /**
     * 根据用户名称获得API URL资源鉴权
     *
     * @param username
     * @return
     */
    @SelectProvider(type = BackStageApiProvider.class,method = "getBackStageApiByUsername")
    List<TinygrayBackstageApi> getApiUrlByUserName(@Param("username") String username);
}
