package com.tinygray.madison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.entity.TinygrayRole;
import com.tinygray.madison.vo.SysRoleAndPermissionVo;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;

import java.util.List;

public interface SysRoleService extends IService<TinygrayRole> {

    List<String> getRolesByUserName(String username);

    List<TinygrayRole> getAllRole(String keyword);

    boolean addRole(TinygrayRole tinygrayRole);

    boolean delRole(long id);

    List<TinygrayFrontMenu> getRoleToMenuList(Integer roleId);
}
