package com.tinygray.madison.service.serviceImpl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tinygray.madison.components.BCryptPasswordEncoderUtil;
import com.tinygray.madison.enmu.ErrorCode;
import com.tinygray.madison.entity.TinygrayMailRetrieve;
import com.tinygray.madison.entity.TinygrayUser;
import com.tinygray.madison.dto.MailDto;
import com.tinygray.madison.mapper.SysUserMapper;
import com.tinygray.madison.service.SysMailRetrieveService;
import com.tinygray.madison.service.SysUserService;
import com.tinygray.madison.util.CommonUtils;
import com.tinygray.madison.util.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.Map;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper,TinygrayUser> implements SysUserService {

    private static final int USER_STATUS = 0;
    private static final int USER_STATUS_Disable = 1;
    private static final String ADMIN = "admin";
    @Autowired
    BCryptPasswordEncoderUtil bCryptPasswordEncoderUtil;

    @Autowired
    SysMailRetrieveService sysMailRetrieveService;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    MailDto mailDto;

    /**
     *
     * @param to
     * @param text
     * @param sentDate
     */
    public void sendEmail(String to, String text, Date sentDate) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setSubject(mailDto.getSubject());
        simpleMailMessage.setFrom(mailDto.getFrom());
        simpleMailMessage.setTo(to);
        simpleMailMessage.setText(text);
        simpleMailMessage.setSentDate(sentDate);
        javaMailSender.send(simpleMailMessage);
    }

    /**
     *
     * @param basePath
     * @param email
     * @return
     */
    @Override
    public R findUserByMail(String basePath, String email) {
        TinygrayUser tinygrayUser = getUserByUserName("", email);
        if (tinygrayUser == null) {
            return R.failed(ErrorCode.EMAIL_ACCOUNT_DOES_NOT_EXIST);
        }else {
            String key = RandomUtils.getRandom(6) + ""; //生成邮件url唯一地址
           /* Timestamp outDate = new Timestamp(System.currentTimeMillis() + (long) (30 * 60 * 1000));//30分钟后过期 忽略毫秒数
            long outTimes = outDate.getSysUpTime();*/
            long outTimes = System.currentTimeMillis() + (long) (30 * 60 * 1000);//30分钟后过期 忽略毫秒数
            String sid = tinygrayUser.getNickname() + "&" + key + "&" + outTimes;
            TinygrayMailRetrieve mailRetrieve = new TinygrayMailRetrieve(tinygrayUser.getNickname(), CommonUtils.EncoderByMd5(sid), outTimes);
            TinygrayMailRetrieve tinygrayMailRetrieve = sysMailRetrieveService.getDataByAccount(tinygrayUser.getNickname());
            if (tinygrayMailRetrieve != null) {
                sysMailRetrieveService.removeById(tinygrayMailRetrieve.getId());
            }
            boolean save = sysMailRetrieveService.save(mailRetrieve);
            if (save) {
                String result = basePath + "user/reset_password?sid=" + CommonUtils.EncoderByMd5(sid) + "&account=" + tinygrayUser.getNickname();
                sendEmail(email, result, DateUtil.date(System.currentTimeMillis()));//发送邮件
                return R.restResult(result, ErrorCode.EMAIL_RETRIEVE_PASSWORD);
            }else {
                return R.ok(false);
            }
        }
    }

    /**
     *
     * @param sid
     * @param account
     * @return
     */
    @Override
    public R verifyMail(String sid, String account) {
        TinygrayMailRetrieve tinygrayMailRetrieve = sysMailRetrieveService.getDataByAccount(account);
        long outTime = tinygrayMailRetrieve.getOutTime();
        /*Timestamp outDate = new Timestamp(System.currentTimeMillis());
        long nowTime=outDate.getSysUpTime();*/
        long nowTime = System.currentTimeMillis();
        System.out.println("nowTime:"+nowTime);
        if (outTime <= nowTime) {
            return R.failed(ErrorCode.EMAIL_TIME_HAS_EXPIRED);
        } else if ("".equals(sid)) {
            return R.failed(ErrorCode.EMAIL_SID_IS_INCOMPLETE_CONTENT);
        } else if (!sid.equals(tinygrayMailRetrieve.getSid())) {
            return R.failed(ErrorCode.EMAIL_SID_IS_ERROR);
        } else {
            return R.restResult(true, ErrorCode.EMAIL_NO_ERROR);
        }
    }

    /**
     *
     * @param username
     * @param email
     * @return
     */
    public TinygrayUser getUserByUserName(String username, String email) {
       /* QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("nickname", username);
        queryWrapper.eq("status", USER_STATUS);*/
        LambdaQueryWrapper<TinygrayUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if ("".equals(email)) {
            lambdaQueryWrapper.eq(TinygrayUser::getNickname, username);
        }else {
            lambdaQueryWrapper.eq(TinygrayUser::getEmail, email);
        }
        lambdaQueryWrapper.eq(TinygrayUser::getStatus, USER_STATUS);
        return this.getOne(lambdaQueryWrapper);
    }

    /**
     *
     * @param user
     * @return
     * @throws Exception
     */
    @Override
    public boolean register(final TinygrayUser user) throws Exception {
        if (user != null) {
            user.setPassword(bCryptPasswordEncoderUtil.encode(user.getPassword()));
            if (user.getId() == 0) {
                TinygrayUser tinygrayUser = this.getUserByUserName(user.getNickname(), "");
                if (tinygrayUser != null) {
                    if (user.getNickname().equals(tinygrayUser.getNickname())) {
                        throw new Exception(ErrorCode.REGISTER_USER_ALREADY_EXISTS.toString());
                    } else if (user.getEmail().equals(tinygrayUser.getEmail())) {
                        throw new Exception(ErrorCode.REGISTER_EMAIL_REGISTERED.toString());
                    }
                }
                return this.save(user);//保存到数据库
            } else {
                return this.saveOrUpdate(user);//保存到数据库
            }
        } else {
            throw new Exception(ErrorCode.REGISTER_USER_OBJECT_IS_EMPTY.toString());
        }
    }

    public boolean save(Map<String,Object> map) {
        TinygrayUser tinygrayUser = new TinygrayUser();
        String username = map.get("user").toString();
        String password = map.get("pass").toString();
        String email = map.get("email").toString();
        tinygrayUser.setNickname(username);
        tinygrayUser.setPassword(bCryptPasswordEncoderUtil.encode(password));   //对密码进行加密
        tinygrayUser.setEmail(email);
        tinygrayUser.setCreateTime(DateUtil.now());
        return this.saveOrUpdate(tinygrayUser);
    }

    /**
     *
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    @Override
    public boolean checkLogin(String username, String password) throws Exception {
        TinygrayUser tinygrayUser = this.getUserByUserName(username, "");
        if (tinygrayUser == null) {
//            throw new Exception("账号不存在，请重新尝试！");
            throw new Exception(ErrorCode.LOGIN_ACCOUNT_DOES_NOT_EXIST.toString());
        }else {
            String encodedPassword = tinygrayUser.getPassword();//加密的密码
            if (!bCryptPasswordEncoderUtil.matches(password, encodedPassword)) {//和加密后的密码进行比配
                throw new Exception(ErrorCode.LOGIN_WRONG_PASSWORD.toString());
            }else {
                return true;
            }
        }
    }

    /**
     * 查询所有用户
     * @param page
     * @param keyword
     * @return
     */
    @Override
    public IPage<TinygrayUser> getAllUser(Page<TinygrayUser> page, String keyword) {
        LambdaQueryWrapper<TinygrayUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.ne(TinygrayUser::getNickname, ADMIN);
        lambdaQueryWrapper.eq(TinygrayUser::getStatus, USER_STATUS);
        if (!StrUtil.isEmpty(keyword)) {
            lambdaQueryWrapper.like(TinygrayUser::getNickname, keyword);
            lambdaQueryWrapper.or();
            lambdaQueryWrapper.eq(TinygrayUser::getEmail, keyword);
        }

        return this.baseMapper.selectPage(page, lambdaQueryWrapper);
    }

    /**
     * 删除用户 根据用户id
     * @param id
     * @return
     */
    @Override
    public boolean delUser(String id) {
        LambdaUpdateWrapper<TinygrayUser> LambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        LambdaUpdateWrapper.eq(TinygrayUser::getId, Long.parseLong(id));
        LambdaUpdateWrapper.set(TinygrayUser::getStatus, USER_STATUS_Disable);
        return this.update(LambdaUpdateWrapper);
    }
}
