package com.tinygray.madison.controller.aliPay.starter.domain.PublicPay;

import com.sun.istack.internal.NotNull;
import com.tinygray.madison.controller.aliPay.domain.PublicPay.RefundQueryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: qlh
 * @Desctiption:
 * @Date: 2020/7/25 11:19
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RefundQueryForm extends RefundQueryDTO {


    @NotNull
    @Override
    public String getOutRequestNo() {
        return super.getOutRequestNo();
    }
}
