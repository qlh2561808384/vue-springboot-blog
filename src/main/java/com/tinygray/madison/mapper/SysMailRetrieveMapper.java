package com.tinygray.madison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tinygray.madison.entity.TinygrayMailRetrieve;

public interface SysMailRetrieveMapper extends BaseMapper<TinygrayMailRetrieve> {
}
