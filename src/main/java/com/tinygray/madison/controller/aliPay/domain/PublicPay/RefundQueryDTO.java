package com.tinygray.madison.controller.aliPay.domain.PublicPay;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @Author: qlh
 * @Desctiption: 退款订单查询实体类
 * @Date: 2020/7/24 23:09
 */
@Data
public class RefundQueryDTO {

    @JSONField(name = "trade_no")
    private String tradeNo;
    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    @JSONField(name = "out_request_no")
    private String outRequestNo;

}
