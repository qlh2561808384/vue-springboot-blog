package com.tinygray.madison.service;

import com.tinygray.madison.entity.TinygrayArticles;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文章表 服务类
 * </p>
 *
 * @author madison
 * @since 2020-08-17
 */
public interface SysTinygrayArticlesService extends IService<TinygrayArticles> {

}
