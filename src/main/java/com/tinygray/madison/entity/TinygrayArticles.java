package com.tinygray.madison.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 文章表
 * </p>
 *
 * @author madison
 * @since 2020-08-17
 */
@Data
//@SuppressWarnings("serial")//禁止少了serialVersionUID时候出现的警告
@EqualsAndHashCode(callSuper = false)//表示重写equals跟hashCode方法
@Accessors(chain = true)//表示树形的set方法返回本实体类对象
@ApiModel(value="TinygrayArticles对象", description="文章表")
public class TinygrayArticles extends Model<TinygrayArticles> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文章主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "文章标题")
    private String articleTitle;

    @ApiModelProperty(value = "文章内容")
    private String articleContent;

    @ApiModelProperty(value = "文章浏览器")
    private Integer articleViews;

    @ApiModelProperty(value = "文章评论总数")
    private Integer articleCommentCount;

    @ApiModelProperty(value = "文章发表日期")
    private Date articleDate;

    @ApiModelProperty(value = "文章点赞数")
    private Integer articleLikeCount;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
