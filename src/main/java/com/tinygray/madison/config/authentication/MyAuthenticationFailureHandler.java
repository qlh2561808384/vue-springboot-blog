package com.tinygray.madison.config.authentication;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.api.IErrorCode;
import com.baomidou.mybatisplus.extension.api.R;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录失败操作
 */
@Component
public class MyAuthenticationFailureHandler extends JSONAuthentication implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException e) throws IOException, ServletException {
        JSONObject jsonObject = JSONUtil.parseObj(e.getMessage());
        long code = Long.parseLong(jsonObject.get("code").toString());
        String msg = jsonObject.get("msg").toString();
        R<String> data = R.failed(new IErrorCode() {
            @Override
            public long getCode() {
                return code;
            }

            @Override
            public String getMsg() {
                return msg;
            }
        });
//        R<String> data = R.failed("登录失败:"+e.getMessage());
        //输出
        this.WriteJSON(request, response, data);
    }
}
