package com.tinygray.madison.controller.aliPay.api;

import com.alipay.api.response.*;
import com.tinygray.madison.controller.aliPay.domain.PagePay.*;

/**
 * @Author: qlh
 * @Desctiption: 支付宝网站支付接口
 * @Date: 2020/7/24 18:22
 */
public interface PCPagePay {
    /**
     * @Author: white
     * @Desctiption:
     * 统一收单下单并支付页面接口
     * @Date: 2018/7/24 21:13
     */
    AlipayTradePagePayResponse pay(PagePayDTO pagePayDTO);

}
