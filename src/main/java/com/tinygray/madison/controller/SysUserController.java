package com.tinygray.madison.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IOUtils;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tinygray.madison.entity.TinygrayUser;
import com.tinygray.madison.service.SysCaptchaService;
import com.tinygray.madison.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("user")
@Api(tags = "用户相关控制器")
public class SysUserController extends ApiController {

    @Autowired
    SysUserService sysUserService;
    @Autowired
    SysCaptchaService sysCaptchaService;

    @ApiOperation(value = "用户注册")
    @PostMapping("register")
    public R Register(@RequestBody(required = false) TinygrayUser tinygrayUser) {
        try {
            return success(sysUserService.register(tinygrayUser));
        } catch (Exception e) {
            e.printStackTrace();
            return success(false);
        }
    }

    @GetMapping("captcha.jpg")
    public void captcha(HttpServletResponse response, String uuid) throws Exception {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");
        //获取图片验证码
        BufferedImage image = sysCaptchaService.getCaptcha(uuid);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
        IOUtils.closeQuietly(out);
    }

    @ApiOperation(value = "根据邮箱修改密码")
    @PostMapping("getPasswordByEmail")
    public R getPasswordByEmail(HttpServletRequest request, @RequestBody Map<String, Object> map) {
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
        if (map.isEmpty()) {
            return failed("请输入邮箱");
        } else {
            return sysUserService.findUserByMail(basePath, map.get("email").toString());
        }
    }

    @ApiOperation(value = "用户邮箱连接跳转地址")
    @GetMapping("reset_password")
    public R verifyMail(String sid, String account) {
        return sysUserService.verifyMail(sid, account);
    }

    @ApiOperation(value = "获取所有用户接口")
    @GetMapping("getAllUser")
    public R getAllUser(Page<TinygrayUser> page, @Param("keyword") String keyword) {
        IPage<TinygrayUser> allUserList = sysUserService.getAllUser(page, keyword);
        return success(allUserList);
    }

    @ApiOperation(value = "删除用户")
    @GetMapping("delUser/{id}")
    public R delUser(@PathVariable("id") String id) {
        return success(sysUserService.delUser(id));
    }
}
