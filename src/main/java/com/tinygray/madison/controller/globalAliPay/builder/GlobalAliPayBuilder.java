package com.tinygray.madison.controller.globalAliPay.builder;

import com.tinygray.madison.controller.globalAliPay.AliPay.AlipaySubmit;
import com.tinygray.madison.controller.globalAliPay.domain.GlobalAlipayVo;
import lombok.Setter;

import java.util.Objects;

/**
 * @Author: qlh
 * @Desctiption:
 * @Date: 2018/7/25 10:22
 */
public class GlobalAliPayBuilder {


    private static GlobalAliPayBuilder globalAliPayBuilder;

    @Setter
    private GlobalAlipayVo globalAlipayVo;

    private GlobalAliPayBuilder(){}

    public static final GlobalAliPayBuilder builder(GlobalAlipayVo properties){
        Objects.requireNonNull(properties," properties must be not null");
        if(globalAliPayBuilder == null){
            globalAliPayBuilder = new GlobalAliPayBuilder();
        }
        globalAliPayBuilder.setGlobalAlipayVo(properties);
        return globalAliPayBuilder;
    }

    public void prepareAliPaySubmit(){
        AlipaySubmit.setGlobalAlipayVo(this.globalAlipayVo);
    }

}
