package com.tinygray.madison.controller.aliPay.domain.PublicPay;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @Author: qlh
 * @Desctiption: 退款订单实体类
 * @Date: 2020/7/24 23:01
 */
@Data
public class RefundDTO {


    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    @JSONField(name = "trade_no")
    private String tradeNo;
    @JSONField(name = "refund_amount")
    private String refundAmount;
    @JSONField(name = "refund_currency")
    private String refundCurrency;
    @JSONField(name = "refund_reason")
    private String refundReason;
    @JSONField(name = "out_request_no")
    private String outRequestNo;
    @JSONField(name = "operator_id")
    private String operatorId;
    @JSONField(name = "store_id")
    private String storeId;
    @JSONField(name = "terminal_id")
    private String terminalId;
    @JSONField(name = "goods_detail")
    private String goodsDetail;
    @JSONField(name = "refund_royalty_parameters")
    private String refundRoyaltyParameters;
}
