package com.tinygray.madison.util;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class MenuTreeUtil {
//    public static Map<String,Object> mapArray = new LinkedHashMap<String, Object>();
    public List<TinygrayFrontMenu> menuCommon;
    private static final int LEVEL = 1;
    private List<TinygrayFrontMenu> TinyMenuCommon;
//    public List<Object> list = new ArrayList<Object>();

    public List<Object> menuList(List<TinygrayFrontMenu> menu){
        this.menuCommon = menu;
        List<Object> list = new ArrayList<Object>();
        for (TinygrayFrontMenu FrontMenu : menu) {
            Map<String,Object> mapArr = new LinkedHashMap<String, Object>();
            if ((FrontMenu.getPid() == 0)) {
                mapArr.put("id", FrontMenu.getId());
                mapArr.put("path", FrontMenu.getPath());
                mapArr.put("name", FrontMenu.getFrontMenuName());
                mapArr.put("label", FrontMenu.getDescription());
                mapArr.put("pid", FrontMenu.getPid());
                mapArr.put("parentLable", FrontMenu.getDescription());
                mapArr.put("icon", FrontMenu.getIcon());
                mapArr.put("url", FrontMenu.getFrontMenuUrl());
                mapArr.put("type", FrontMenu.getType());
                mapArr.put("sort", FrontMenu.getFrontMenuSort());
                List<?> menuChild = menuChild(FrontMenu);
                if (!menuChild.isEmpty()) {
                    mapArr.put("children", menuChild);
                }
                list.add(mapArr);
            }
        }
        return list;
    }

    public List<?> menuChild(TinygrayFrontMenu Menu){
        List<Object> lists = new ArrayList<Object>();
        for(TinygrayFrontMenu tinygrayFrontMenu:menuCommon){
            Map<String,Object> childArray = new LinkedHashMap<String, Object>();
            if (tinygrayFrontMenu.getPid() == Menu.getId()) {
                childArray.put("id", tinygrayFrontMenu.getId());
                childArray.put("path", tinygrayFrontMenu.getPath());
                childArray.put("name", tinygrayFrontMenu.getFrontMenuName());
                childArray.put("label", tinygrayFrontMenu.getDescription());
                childArray.put("pid", tinygrayFrontMenu.getPid());
                childArray.put("parentLable", Menu.getDescription());
                childArray.put("icon", tinygrayFrontMenu.getIcon());
                childArray.put("url", tinygrayFrontMenu.getFrontMenuUrl());
                childArray.put("type", tinygrayFrontMenu.getType());
                childArray.put("sort", tinygrayFrontMenu.getFrontMenuSort());
                List<?> menuChild = menuChild(tinygrayFrontMenu);
                if (!menuChild.isEmpty()) {
                    childArray.put("children", menuChild(tinygrayFrontMenu));
                }
                lists.add(childArray);
            }
        }
        return lists;
    }

    public List<TinygrayFrontMenuVo> selectMenusListForTree(List<TinygrayFrontMenu> menu){
        this.TinyMenuCommon = menu;
        List<TinygrayFrontMenuVo> list = new ArrayList<>();
        for (TinygrayFrontMenu FrontMenu : menu) {
            TinygrayFrontMenuVo tinygrayFrontMenuVo = new TinygrayFrontMenuVo();
            if ((FrontMenu.getPid() == 0)) {
                tinygrayFrontMenuVo.setId(FrontMenu.getId());
                tinygrayFrontMenuVo.setPath(FrontMenu.getPath());
                tinygrayFrontMenuVo.setFrontMenuName(FrontMenu.getFrontMenuName());
                tinygrayFrontMenuVo.setDescription(FrontMenu.getDescription());
                tinygrayFrontMenuVo.setPid(FrontMenu.getPid());
                tinygrayFrontMenuVo.setParentName("");
                tinygrayFrontMenuVo.setIcon("el-icon-" + FrontMenu.getIcon());
                tinygrayFrontMenuVo.setFrontMenuUrl(FrontMenu.getFrontMenuUrl());
                tinygrayFrontMenuVo.setType(FrontMenu.getType());
                tinygrayFrontMenuVo.setFrontMenuSort(FrontMenu.getFrontMenuSort());
                tinygrayFrontMenuVo.set_level(LEVEL);
                List<?> menuChild = selectMenusListForTreeChild(FrontMenu);
                if (!menuChild.isEmpty()) {
                    tinygrayFrontMenuVo.setChildren((List<TinygrayFrontMenuVo>) menuChild);
                }
                list.add(tinygrayFrontMenuVo);
            }
        }
        return list;
    }

    public List<?> selectMenusListForTreeChild(TinygrayFrontMenu Menu){
        List<Object> lists = new ArrayList<Object>();
        for(TinygrayFrontMenu tinygrayFrontMenu:TinyMenuCommon){
            TinygrayFrontMenuVo tinygrayFrontMenuVo = new TinygrayFrontMenuVo();
            if (tinygrayFrontMenu.getPid() == Menu.getId()) {
                tinygrayFrontMenuVo.setId(tinygrayFrontMenu.getId());
                tinygrayFrontMenuVo.setPath(tinygrayFrontMenu.getPath());
                tinygrayFrontMenuVo.setFrontMenuName(tinygrayFrontMenu.getFrontMenuName());
                tinygrayFrontMenuVo.setDescription(tinygrayFrontMenu.getDescription());
                tinygrayFrontMenuVo.setPid(tinygrayFrontMenu.getPid());
                tinygrayFrontMenuVo.setParentName(Menu.getDescription());
                tinygrayFrontMenuVo.setIcon("el-icon-" + tinygrayFrontMenu.getIcon());
                tinygrayFrontMenuVo.setFrontMenuUrl(tinygrayFrontMenu.getFrontMenuUrl());
                tinygrayFrontMenuVo.setType(tinygrayFrontMenu.getType());
                tinygrayFrontMenuVo.setFrontMenuSort(tinygrayFrontMenu.getFrontMenuSort());
                tinygrayFrontMenuVo.set_level(LEVEL + 1);
                List<?> menuChild = selectMenusListForTreeChild(tinygrayFrontMenu);
                if (!menuChild.isEmpty()) {
                    tinygrayFrontMenuVo.setChildren((List<TinygrayFrontMenuVo>) tinygrayFrontMenu);
                }
                lists.add(tinygrayFrontMenuVo);
            }
        }
        return lists;
    }
}
