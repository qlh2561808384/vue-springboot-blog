package com.tinygray.madison.controller.aliPay.domain.PublicPay;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @Author: qlh
 * @Desctiption: 支付宝支付查询实体类
 * @Date: 2020/7/24 23:03
 */
@Data
public class AliPayQueryDTO {

    @JSONField(name = "out_trade_no")
    private String outTradeNo;

    @JSONField(name = "trade_no")
    private String tradeNo;


}
