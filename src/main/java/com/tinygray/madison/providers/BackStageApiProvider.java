package com.tinygray.madison.providers;

import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class BackStageApiProvider {
    /**
     * 根据用户名称获得API URL资源鉴权
     * @param params
     * @return
     */
    public String getBackStageApiByUsername(Map<String, Object> params){
        return new SQL(){
            {
                SELECT_DISTINCT("api.backstage_api_url","api.backstage_api_method");
                FROM("tinygray_user user",
                        "tinygray_role role",
                        "tinygray_user_role user_role",
                        "tinygray_backstage_api api",
                        "tinygray_role_backstage_api role_api");
                WHERE("api.id = role_api.backstage_api_id",
                        "api.backstage_api_url <> 'none'",
                        "role.id = role_api.roleid",
                        "user.id = user_role.userid",
                        "role.id = user_role.roleid",
                        "user.nickname = #{username}");
            }
        }.toString();
    }

    /**
     * 根据登录账号，获得前端展现的菜单
     * 控制前端菜单的权限
     *
     * @param params
     * @return
     */
    public String getMenusByUserName(Map<String, Object> params) {
        return new SQL() {
            {
                SELECT_DISTINCT("a.*");
                FROM("tinygray_front_menu a"
                        , "tinygray_role b"
                        , "tinygray_role_front_menu c"
                        , "tinygray_user d"
                        , "tinygray_user_role e");
                WHERE("(a.id = c.front_menu_id or c.front_menu_id = '*')"
                        , "b.id = c.roleid"
                        , "d.id = e.userid"
                        , "e.roleid = c.roleid"
                        , "d.nickname = #{username}");
                ORDER_BY("a.front_menu_sort asc");
            }
        }.toString();
    }
}
