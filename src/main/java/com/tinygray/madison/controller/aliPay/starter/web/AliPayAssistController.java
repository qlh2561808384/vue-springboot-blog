package com.tinygray.madison.controller.aliPay.starter.web;

import com.alipay.api.response.*;
import com.tinygray.madison.controller.aliPay.api.AliPayAssist;
import com.tinygray.madison.controller.aliPay.starter.domain.PublicPay.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Class: AliPayAssistController
 * @Description: 支付宝支付交易辅助控制器
 * @title: AliPayAssistController
 * @Author qlh
 * @Date: 2020/10/30 14:39
 * @Version 1.0
 */
@RestController
@Api(tags = "ali-pay-trade-Assist")
@RequestMapping("/alipay/assist")
public class AliPayAssistController {
    @Autowired
    AliPayAssist aliPayAssist;

    @GetMapping(value = "/query")
    public ResponseEntity query(AliPayQueryForm aliPayQueryForm){
        AlipayTradeQueryResponse response = aliPayAssist.query(aliPayQueryForm);
        return ResponseEntity.ok().body(response.getBody());
    }

    @PostMapping(value = "/refund")
    public ResponseEntity refund(@Validated @RequestBody RefundForm refundForm){
        AlipayTradeRefundResponse response = aliPayAssist.refund(refundForm);
        return ResponseEntity.ok().body(response.getBody());
    }

    @GetMapping(value = "/refund/query")
    public ResponseEntity refundQuery(@Validated RefundQueryForm refundQueryForm){
        AlipayTradeFastpayRefundQueryResponse response = aliPayAssist.refundQuery(refundQueryForm);
        return ResponseEntity.ok().body(response.getBody());
    }


    @PostMapping(value = "/close")
    public ResponseEntity close(@RequestBody OrderCloseForm orderCloseForm){
        AlipayTradeCloseResponse response = aliPayAssist.close(orderCloseForm);
        return ResponseEntity.ok().body(response.getBody());
    }

    @GetMapping(value = "/query/BillDownloadUrl")
    public ResponseEntity queryBillDownloadUrl(@Validated BillDownloadUrlFrom billDownloadUrlFrom) {
        AlipayDataDataserviceBillDownloadurlQueryResponse response = aliPayAssist.queryBillDownloadUrl(billDownloadUrlFrom);
        return ResponseEntity.ok().body(response.getBody());
    }

}
