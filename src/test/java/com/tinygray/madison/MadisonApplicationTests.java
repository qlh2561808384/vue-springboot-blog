package com.tinygray.madison;

import cn.hutool.core.math.MathUtil;
import cn.hutool.core.util.RandomUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Date;

@SpringBootTest
class MadisonApplicationTests {

    @Autowired
    JavaMailSender javaMailSender;

    @Test
    void contextLoads() {
    }

    @Test
    public void sendSimpleMail() {
        //1.构建一个邮件对象
        SimpleMailMessage message = new SimpleMailMessage();
        //2.设置邮件主题
        message.setSubject("这是一封测试邮件");
        //3.设置邮件发送者
        message.setFrom("qlh2561808384@163.com");
        //4. 设置邮件接收者，可以有多个接收者
        message.setTo("2561808384@qq.com");
        //5.设置邮件抄送人，可以有多个抄送人
//        message.setCc("371820637@qq.com");
        //6.设置隐秘抄送人，可以有多个
//        message.setBcc("1406134098@qq.com");
        //7.设置邮件发送日期
        message.setSentDate(new Date());
        //8. 设置邮件的正文
        message.setText("这是测试邮件的正文");
        //9. 发送邮件
        javaMailSender.send(message);
    }
    @Test
    public void getCode(){
        System.out.println(RandomUtil.randomNumbers(6));
    }

}
