package com.tinygray.madison.controller.aliPay.starter.domain.PublicPay;

import com.tinygray.madison.controller.aliPay.domain.PublicPay.BillDownloadUrlDTO;

/**
 * @Class: BillDownloadUrlForm
 * @Description: BillDownloadUrlFrom$
 * @title: BillDownloadUrlFrom
 * @Author qlh
 * @Date: 2020/10/28 16:41
 * @Version 1.0
 */
public class BillDownloadUrlFrom extends BillDownloadUrlDTO {

}
