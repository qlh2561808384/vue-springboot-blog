package com.tinygray.madison.vo;

import com.tinygray.madison.entity.TinygrayFrontMenu;

import java.util.List;

/**
 * @Class: TinygrayFrontMenuVo
 * @Description: TinygrayFrontMenuVo$
 * @title: TinygrayFrontMenuVo
 * @Author qlh
 * @Date: 2020/9/16 9:42
 * @Version 1.0
 */
public class TinygrayFrontMenuVo extends TinygrayFrontMenu {

    private List<TinygrayFrontMenuVo> children;

    private Integer _level;

    public Integer get_level() {
        return _level;
    }

    public void set_level(Integer _level) {
        this._level = _level;
    }

    public List<TinygrayFrontMenuVo> getChildren() {
        return children;
    }

    public void setChildren(List<TinygrayFrontMenuVo> children) {
        this.children = children;
    }
}
