package com.tinygray.madison.controller.aliPay.starter.domain.AppPay;

import com.tinygray.madison.controller.aliPay.domain.AppPay.AppPayDTO;

/**
 * @Class: AppPayForm
 * @Description: AppPayForm$
 * @title: AppPayForm
 * @Author qlh
 * @Date: 2020/10/29 15:44
 * @Version 1.0
 */
public class AppPayForm extends AppPayDTO {
}
