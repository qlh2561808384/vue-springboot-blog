package com.tinygray.madison.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tinygray.madison.entity.TinygrayTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文章标签表 服务类
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
public interface SysTinygrayTagService extends IService<TinygrayTag> {

    IPage<TinygrayTag> getTagList(Page<TinygrayTag> page, String keyword);

    boolean saveTag(TinygrayTag tinygrayTag);
}
