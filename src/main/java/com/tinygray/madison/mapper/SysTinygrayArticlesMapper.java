package com.tinygray.madison.mapper;

import com.tinygray.madison.entity.TinygrayArticles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文章表 Mapper 接口
 * </p>
 *
 * @author madison
 * @since 2020-08-17
 */
public interface SysTinygrayArticlesMapper extends BaseMapper<TinygrayArticles> {

}
