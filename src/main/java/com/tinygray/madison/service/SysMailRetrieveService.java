package com.tinygray.madison.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tinygray.madison.entity.TinygrayMailRetrieve;

public interface SysMailRetrieveService extends IService<TinygrayMailRetrieve> {

    TinygrayMailRetrieve getDataByAccount(String Account);

}
