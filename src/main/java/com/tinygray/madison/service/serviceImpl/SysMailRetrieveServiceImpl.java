package com.tinygray.madison.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tinygray.madison.entity.TinygrayMailRetrieve;
import com.tinygray.madison.mapper.SysMailRetrieveMapper;
import com.tinygray.madison.service.SysMailRetrieveService;
import org.springframework.stereotype.Service;

@Service
public class SysMailRetrieveServiceImpl extends ServiceImpl<SysMailRetrieveMapper, TinygrayMailRetrieve> implements SysMailRetrieveService {
    @Override
    public TinygrayMailRetrieve getDataByAccount(String Account) {
        LambdaQueryWrapper<TinygrayMailRetrieve> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(TinygrayMailRetrieve::getAccount, Account);
        return this.getOne(lambdaQueryWrapper);
    }
}
