package com.tinygray.madison.controller.globalAliPay.api.impl;

import com.tinygray.madison.controller.globalAliPay.AliPay.AlipaySubmit;
import com.tinygray.madison.controller.globalAliPay.api.GlobalPCPay;
import com.tinygray.madison.controller.globalAliPay.domain.GlobalAlipayVo;
import com.tinygray.madison.util.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Class:
 * @Description: GlobalPCPay$
 * @title: GlobalPCPay
 * @Author qlh
 * @Date: 2020/7/24 11:24
 * @Version 1.0
 */
@Service
public class GlobalPCPayImpl implements GlobalPCPay {
    @Autowired
    GlobalAlipayVo globalAlipayVo;
    @Override
    public String globalPCPay() {
        //把请求参数打包成数组
        //package the request parameters
        Map<String, String> sParaTemp = new HashMap<String, String>();
        sParaTemp.put("service",globalAlipayVo.getService());
        sParaTemp.put("partner",globalAlipayVo.getPartner());
        sParaTemp.put("_input_charset", globalAlipayVo.getCharset());
        sParaTemp.put("notify_url", globalAlipayVo.getNotifyUrl());
        sParaTemp.put("return_url", globalAlipayVo.getReturnUrl());
        sParaTemp.put("out_trade_no", CommonUtils.generateUUID());
        sParaTemp.put("subject", "qlh");
        sParaTemp.put("total_fee", "1");
        //sParaTemp.put("rmb_fee", rmb_fee);
        sParaTemp.put("body", "test");//body商品描述 可为空
        sParaTemp.put("currency", "USD");//currency 币种，不可空
        sParaTemp.put("product_code", globalAlipayVo.getProduct_code());//注意：必传，PC端是NEW_OVERSEAS_SELLER，移动端是NEW_WAP_OVERSEAS_SELLER Remarks:Mandatory.For PC: NEW_OVERSEAS_SELLER ;FOR WAP and APP: NEW_WAP_OVERSEAS_SELLER
        //sParaTemp.put("supplier", supplier);
        sParaTemp.put("timeout_rule", globalAlipayVo.getTimeout_rule());
        //split_fund_info = split_fund_info.replaceAll("\"", "'");
        //sParaTemp.put("split_fund_info", split_fund_info);

//            trade_information = trade_information.replaceAll("\"", "'");
        sParaTemp.put("trade_information", globalAlipayVo.getTrade_information().toString());
        String response = AlipaySubmit.buildRequest(sParaTemp, "get", "OK");
        System.out.println(response);
        return response;
    }
}
