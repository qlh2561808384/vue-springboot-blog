package com.tinygray.madison.controller;


import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.tinygray.madison.service.SysTinygrayArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * <p>
 * 文章表 前端控制器
 * </p>
 *
 * @author madison
 * @since 2020-08-17
 */
@RestController
@RequestMapping("/tinygray-articles")
public class SysTinygrayArticlesController extends ApiController {
    @Autowired
    SysTinygrayArticlesService sysTinygrayArticlesService;
    @Value("${upload.path}")
    private String uploadPath;
    @RequestMapping(value = "image", method = RequestMethod.POST)
    public R upload(HttpServletRequest request, @RequestParam Map<String, String> map) {
        String msg = "";
        MultipartHttpServletRequest Murequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> files = Murequest.getFileMap();//得到文件map对象
        if (!map.isEmpty()) {
            String imgName = map.get("imgName");
            int i = imgName.lastIndexOf("/");
            String substring = imgName.substring(i + 1);
            File file = new File(uploadPath + File.separator + substring);
            if (file.exists()) {
                file.delete();
            }
        }
        for(MultipartFile file :files.values()){
            if (file.isEmpty()) {
                // 设置错误状态码
                msg = "failed,file not empty";
                return failed(msg);
            }
            // 拿到文件名
            String newName = null;
            String openfilename = file.getOriginalFilename();
            String filename = openfilename.replace(" ", "_");
            int one = filename.lastIndexOf(".");
            if (-1 == one) {
                msg = "failed,File format error";
                return failed(msg);
            }else {
                newName = filename.replace(filename.substring(0, one), RandomUtil.randomString(10));
            }
            // 存放上传图片的文件夹
            String fileDirPath = new String(uploadPath + File.separator);
            File fileDir = new File(fileDirPath);
            fileDir.delete();
            if(!fileDir.exists()){
                // 递归生成文件夹
                fileDir.mkdirs();
            }
            // 输出文件夹绝对路径  -- 这里的绝对路径是相当于当前项目的路径而不是“容器”路径
//            System.out.println(fileDir.getAbsolutePath());
            try {
                // 构建真实的文件路径
                File newFile = new File(fileDir.getAbsolutePath() + File.separator + newName);
//                System.out.println(newFile.getAbsolutePath());
                // 上传图片到 -》 “绝对路径”
                file.transferTo(newFile);
                msg = "http://localhost:8088/images/" + newName;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return success(msg);
    }

}
