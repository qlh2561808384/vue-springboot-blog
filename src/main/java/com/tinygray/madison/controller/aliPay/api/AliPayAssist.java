package com.tinygray.madison.controller.aliPay.api;

import com.alipay.api.response.*;
import com.tinygray.madison.controller.aliPay.domain.PublicPay.*;

public interface AliPayAssist {
    /**
     * @Author: white
     * @Desctiption:
     * 统一收单线下交易查询接口
     * @Date: 2018/7/25 9:46
     */
    AlipayTradeQueryResponse query(AliPayQueryDTO queryDTO);

    /**
     * @Author: white
     * @Desctiption:
     * 统一收单交易退款接口
     * @Date: 2018/7/25 9:50
     */
    AlipayTradeRefundResponse refund(RefundDTO refundDTO);

    /**
     * @Author: white
     * @Desctiption:
     * 统一收单交易退款查询接口
     * @Date: 2018/7/25 9:50
     */
    AlipayTradeFastpayRefundQueryResponse refundQuery(RefundQueryDTO refundQueryDTO);

    /**
     * @Author: white
     * @Desctiption:
     * 统一收单交易关闭接口
     * @Date: 2018/7/25 9:57
     */
    AlipayTradeCloseResponse close(CloseDTO closeDTO);

    /**
     * @Author: white
     * @Desctiption:   统一收单交易 对账单url下载
     * @Date: 2018/7/25 9:57
     */
    AlipayDataDataserviceBillDownloadurlQueryResponse queryBillDownloadUrl(BillDownloadUrlDTO billDownloadUrlDTO);
}
