package com.tinygray.madison.service;

public interface SmsService {

    boolean sendSms(String phoneNumber, String code);
}
