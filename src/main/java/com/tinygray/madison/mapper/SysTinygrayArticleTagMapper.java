package com.tinygray.madison.mapper;

import com.tinygray.madison.entity.TinygrayArticleTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文章标签关系表 Mapper 接口
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
public interface SysTinygrayArticleTagMapper extends BaseMapper<TinygrayArticleTag> {

}
