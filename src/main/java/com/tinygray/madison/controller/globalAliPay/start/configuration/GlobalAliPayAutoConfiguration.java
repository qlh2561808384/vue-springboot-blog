package com.tinygray.madison.controller.globalAliPay.start.configuration;

import com.tinygray.madison.controller.globalAliPay.builder.GlobalAliPayBuilder;
import com.tinygray.madison.controller.globalAliPay.domain.GlobalAlipayVo;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: qlh
 * @Desctiption: 加载配置信息到spring容器
 * @Date: 2020/7/24 17:37
 */
@Configuration
@ComponentScan("com.tinygray.madison.controller.aliPay.starter")
public class GlobalAliPayAutoConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "global.alipay")
    public GlobalAlipayVo getGlobalAliPayProperties(){
        return new GlobalAlipayVo();
    }
    @Bean
    public void getGlobalAliPayBuild(){
        GlobalAliPayBuilder.builder(getGlobalAliPayProperties()).prepareAliPaySubmit();
    }
}
