package com.tinygray.madison.controller.aliPay.domain.PublicPay;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @Author: qlh
 * @Desctiption: 支付宝关闭付款实体类
 * @Date: 20120/7/25 9:58
 */
@Data
public class CloseDTO {

    @JSONField(name = "trade_no")
    private String tradeNo;
    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    @JSONField(name = "operator_id")
    private String operatorId;

}
