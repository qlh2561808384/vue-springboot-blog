package com.tinygray.madison.controller.aliPay.starter.configuration;

import com.tinygray.madison.controller.aliPay.api.AppPay;
import com.tinygray.madison.controller.aliPay.api.PCPagePay;
import com.tinygray.madison.controller.aliPay.api.impl.AppPayImpl;
//import com.tinygray.madison.controller.aliPay.builder.AliPayBuilder;
import com.tinygray.madison.controller.aliPay.config.AliPayProperties;
import com.tinygray.madison.controller.aliPay.util.AliPaySignUtil;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: qlh
 * @Desctiption: 加载配置信息到spring容器
 * @Date: 2020/7/24 17:37
 */
/*@Configuration
@ComponentScan("com.tinygray.madison.controller.aliPay.starter")
public class AliPayAutoConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "alipay")
    public AliPayProperties getAliPayProperties(){
        return new AliPayProperties();
    }

    @Bean
    public PCPagePay getPCPagePay(){
        return  AliPayBuilder.builder(getAliPayProperties()).preparePcPagePay();
    }

    @Bean
    public AppPay getAppPay(){
        return  AliPayBuilder.builder(getAliPayProperties()).prepareAppPay();
    }

    @Bean
    public AliPaySignUtil getSignUtil(){
        return  AliPayBuilder.builder(getAliPayProperties()).prepareSignUtil();
    }

}*/
