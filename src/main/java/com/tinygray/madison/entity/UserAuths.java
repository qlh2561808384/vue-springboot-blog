package com.tinygray.madison.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAuths implements Serializable {

  private long id;
  private long userId;
  private String identityType;
  private String identifier;
  private String credential;
}
