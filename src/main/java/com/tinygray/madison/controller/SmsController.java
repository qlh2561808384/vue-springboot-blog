package com.tinygray.madison.controller;

import cn.hutool.core.util.RandomUtil;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.tinygray.madison.enmu.ErrorCode;
import com.tinygray.madison.service.SmsService;
import com.tinygray.madison.service.redis.RedisKeyConstants;
import com.tinygray.madison.service.redis.RedisService;
import com.tinygray.madison.util.CommonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @Class: SmsController
 * @Description: SmsController$
 * @title: SmsController
 * @Author qlh
 * @Date: 2020/9/10 10:30
 * @Version 1.0
 */
@RestController
@Api(tags = {"短信Controller"})
@RequestMapping("sms/ali")
public class SmsController extends ApiController {
    @Autowired
    SmsService smsService;
    @Autowired
    RedisService redisService;
    /**
     * 验证码过期时长5秒
     */
    private final static long VERIFICATION_CODE_EXPIRE = 60 * 5;
    /**
     * 产品名称:云通信短信API产品,开发者无需替换
     */
    static final String product = "Dysmsapi";
    /**
     * 产品域名,开发者无需替换
     */
    static final String domain = "dysmsapi.aliyuncs.com";

    /**
     * TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找，上方申请的)
     */
    static final String accessKeyId = "LTAI4G2Zi52ySZKgFJVype6f";
    static final String accessKeySecret = "ycos2SJjnn6dm4V1emzrbuiSWuoj1M";

    @GetMapping("getCode")
    @ApiOperation(value = "发送短信功能测试",notes = "阿里云短信功能")
    @ApiIgnore
    public R sendSms() throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers("18259973869");
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("pmd肝纤维检测");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode("SMS_201652002");
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam("{\"name\":\"Tom\", \"code\":\"123\"}");

        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");

        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        //request.setOutId("yourOutId");

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = acsClient.getAcsResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
        }

        return success(sendSmsResponse);
    }

    @GetMapping("/sendCodeSms/{phoneNumber}")
    @ApiOperation("发送短信功能")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phoneNumber",value = "手机号",required = true,paramType = "path",dataType = "String")
    })
    public R SmsSendKaptcha(@PathVariable(value = "phoneNumber") String phoneNumber) {
        String code = RandomUtil.randomNumbers(6);
        boolean flag = smsService.sendSms(phoneNumber, code);
        if (flag) {
            /**
             * 发送成功 将随机验证码放到redis中
             */
            System.out.println("发送短信成功");
            redisService.setEx(RedisKeyConstants.REGISTER_PHONE_VERIFICATION_CODE + CommonUtils.generateUUID(), code, VERIFICATION_CODE_EXPIRE);
            return success(true);
        } else {
            return R.failed(ErrorCode.REGISTER_FAILED_TO_SEND_CODE);
        }
    }
}
