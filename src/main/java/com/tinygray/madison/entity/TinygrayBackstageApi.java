package com.tinygray.madison.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TinygrayBackstageApi {

  private long id;
  private String backstageApiName;
  private String backstageApiUrl;
  private String backstageApiMethod;
  private String description;

}
