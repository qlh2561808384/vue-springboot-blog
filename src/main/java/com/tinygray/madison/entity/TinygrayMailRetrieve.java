package com.tinygray.madison.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TinygrayMailRetrieve {

  private long id;
  private String account;
  private String sid;
  private long outTime;

  public TinygrayMailRetrieve(String account, String sid, long outTime) {
    this.account = account;
    this.sid = sid;
    this.outTime = outTime;
  }

}
