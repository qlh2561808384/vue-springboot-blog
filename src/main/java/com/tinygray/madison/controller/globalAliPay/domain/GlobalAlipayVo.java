package com.tinygray.madison.controller.globalAliPay.domain;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

@Data
//@Component
//@ConfigurationProperties(prefix = "global.alipay")
public class GlobalAlipayVo {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    private String partner;

    // 商户私钥，您的PKCS8格式RSA2私钥
    private String global_merchant_private_key;

    // 支付宝公钥 对应APPID下的支付宝公钥。
    private String global_alipay_private_key;

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    private String notifyUrl;

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    private String returnUrl;

    // 签名方式
    private String global_sign_type;

    // 字符编码格式
    private String charset;

    // 支付宝网关
    private String global_gatewayUrl;

    //该笔订单允许的最晚付款时间
    private String timeout_rule;

    //销售产品码，商家和支付宝签约的产品码，为固定值 create_forex_trade
    private String service;

    private String product_code;

    private Map<String, String> trade_information;

    private String log_path;


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    private  void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
