package com.tinygray.madison.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.entity.TinygrayRole;
import com.tinygray.madison.vo.SysRoleAndPermissionVo;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SysRoleMapper extends BaseMapper<TinygrayRole> {

    @Select("select role.rolename\n" +
            "from tinygray_user user,\n" +
            "     tinygray_role role,\n" +
            "     tinygray_user_role user_role\n" +
            "where user.id = user_role.userid\n" +
            "  and role.id = user_role.roleid\n" +
            "  and user.status = 0\n" +
            "  and role.status = 0\n" +
            "  and user.nickname = #{username}")
    List<String> getRolesByUserName(@Param("username") String username);

    @Select("<script>" + "select * from tinygray_role where status = 0 and rolename != 'ROLE_ADMIN' "
            + "<if test='keyword!=\"\"'> and rolename like \"%\"#{keyword}\"%\" </if>"
            + "</script>")
    List<TinygrayRole> getAllRole(@Param("keyword") String keyword);

    @Update("update tinygray_role set status = 1 where id = #{id}")
    boolean delRole(@Param("id") long id);

    @Select("select t1.id, t1.description, t1.pid, t2.role_id\n" +
            "from tinygray_front_menu t1\n" +
            "         left join (select a.id menuId, a.front_menu_name, a.pid, a.front_menu_sort, b.id role_id\n" +
            "                    from tinygray_front_menu a,\n" +
            "                         tinygray_role b,\n" +
            "                         tinygray_role_front_menu c\n" +
            "                    where a.id = c.front_menu_id\n" +
            "                      and b.id = c.roleid\n" +
            "                      and a.type IS NOT NULL\n" +
            "                      and b.id = 1) t2 on t1.id = t2.menuId\n" +
            "where t1.type IS NOT NULL\n" +
            "order by t1.front_menu_sort asc;")
    List<TinygrayFrontMenu> getRoleToMenuList(@Param("roleId") Integer roleId);
}
