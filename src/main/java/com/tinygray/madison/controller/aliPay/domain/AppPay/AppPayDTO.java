package com.tinygray.madison.controller.aliPay.domain.AppPay;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @Class: AppPayDTO
 * @Description: AppPayDTO$
 * @title: AppPayDTO
 * @Author qlh
 * @Date: 2020/10/29 15:46
 * @Version 1.0
 */
@Data
public class AppPayDTO {
    @JSONField(name = "out_trade_no")
    private String outTradeNo;
    @JSONField(name = "product_code")
    private String productCode = "QUICK_MSECURITY_PAY";
    @JSONField(name = "total_amount")
    private String totalAmount;
    @JSONField(name = "subject")
    private String subject;
    @JSONField(name = "body")
    private String body;
    @JSONField(name = "goods_detail")
    private String goodsDetail;
    @JSONField(name = "passback_params")
    private String passbackParams;
    @JSONField(name = "extend_params")
    private String extendParams;
    @JSONField(name = "goods_type")
    private String goodsType;
    @JSONField(name = "timeout_express")
    private String timeoutExpress = "1h";
    @JSONField(name = "enable_pay_channels")
    private String enablePayChannels;
    @JSONField(name = "disable_pay_channels")
    private String disablePayChannels;
}
