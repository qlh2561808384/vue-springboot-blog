package com.tinygray.madison.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tinygray.madison.entity.TinygrayUser;

public interface SysUserMapper extends BaseMapper<TinygrayUser> {
}
