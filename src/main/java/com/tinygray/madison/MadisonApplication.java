package com.tinygray.madison;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.tinygray.madison.mapper")
public class MadisonApplication {

    public static void main(String[] args) {
        SpringApplication.run(MadisonApplication.class, args);
    }

}
