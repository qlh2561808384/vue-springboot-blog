package com.tinygray.madison.service.serviceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tinygray.madison.entity.TinygrayFrontMenu;
import com.tinygray.madison.mapper.SysTinygrayFrontMenuMapper;
import com.tinygray.madison.service.SysTinygrayFrontMenuService;
import com.tinygray.madison.vo.TinygrayFrontMenuVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 前端菜单表 服务实现类
 * </p>
 *
 * @author madison
 * @since 2020-09-05
 */
@Service
public class SysTinygrayFrontMenuServiceImpl extends ServiceImpl<SysTinygrayFrontMenuMapper, TinygrayFrontMenu> implements SysTinygrayFrontMenuService {
    @Override
    public List<TinygrayFrontMenu> getMenusByUserName(String username) {
        return this.baseMapper.getMenusByUserName(username);
    }

    @Override
    public List<TinygrayFrontMenu> selectMenus() {
        return this.list(new QueryWrapper<TinygrayFrontMenu>().lambda().isNotNull(TinygrayFrontMenu::getType).orderByAsc(TinygrayFrontMenu::getFrontMenuSort));
    }

    @Override
    public TinygrayFrontMenu getMenuById(Integer menuId) {
        return this.getById(menuId);
    }

    @Override
    public boolean deleteMenuById(TinygrayFrontMenuVo tinygrayFrontMenuVo) {
//        this.baseMapper.deleteById()
        return false;
    }
}
