package com.tinygray.madison.service.serviceImpl;

import com.tinygray.madison.entity.TinygrayArticles;
import com.tinygray.madison.mapper.SysTinygrayArticlesMapper;
import com.tinygray.madison.service.SysTinygrayArticlesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章表 服务实现类
 * </p>
 *
 * @author madison
 * @since 2020-08-17
 */
@Service
public class SysTinygrayArticlesServiceImpl extends ServiceImpl<SysTinygrayArticlesMapper, TinygrayArticles> implements SysTinygrayArticlesService {

}
